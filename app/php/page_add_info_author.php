<?php
require_once('page_public.php');

class addInfoAuhtor extends page_public
{
    protected function Content()
    {
        $this->AppendScript("add_authors");
        $this->AppendAssetScript('/js/select2/dist/js/select2.full');
        $this->AppendAssetCss('js/select2/dist/css/select2');
        $author = "        
            <table border=\"0\" align=\"center\">
                <tbody>
                <tr>
                    <td align=\"right\">Имя автора(Name Author)</td>
                    <td><input id=\"authorname\" size=\"40\" type=\"text\"/></td>
                </tr>
                <tr>
                    <td align=\"right\">Аватар(Avatar)</td>
                    <td><input id=\"avatar\" size=\"40\" type=\"file\" accept=\"image/*\"/></td>
                </tr>
                <tr>
                    <td align=\"right\">Отправить(Submit)</td>
                    <td><input id=\"addAuthorButton\" type=\"submit\"></td>
                </tr>
                </tbody>
            </table>
            <div id=\"info\"></div>
        ";
        return $author;
    }
}

$info = new addInfoAuhtor();
$info->DisplayPage();