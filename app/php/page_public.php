<?php
require_once('dataprocessing.php');

// создаем класс для отображения общедоступных страниц скрипта page_user
// и наследуем его от dataprocessing
class page_public extends dataprocessing
{
    // метод для вывода содержания страницы
    public function DisplayPage()
    {
        $meta = $this->MetaTag();
        $menu = $this->Menu();
        $content = $this->Content();
        $output = $meta . $content;
        $this->AppendMenu($menu);
        $this->AppendContent($output);
        $this->OutputHtml();
    }

    // мета теги
    public function MetaTag()
    {

    }

    public function Menu()
    {
        $constant = 'constant';
        $loginUrl = null;
        $regisUrl = null;
        $cabUrl = null;
        if (!isset($_SESSION["username"])) {
            $loginUrl = "<li><a href='" . APP_URL . "/index.php'><div>Вход</div></a></li >";
            $regisUrl = "<li><a href='" . APP_PHP_URL . "/page_registration.php'><div>Форма регистрации</div></a></li>";
        } else {
            $cabUrl = "<li><a href='" . APP_PHP_USER_URL . "/index.php'><div>Личный кабинет</div></a></li>";
        }
        $output = <<<ABC
        <div class="menu">
            <ul>
                {$loginUrl}
                {$regisUrl}
                {$cabUrl}
                <li><a href='{$constant("APP_PHP_URL")}/page_search_public.php'><div>Поиск</div></a></td>
                <li><a href='{$constant("APP_PHP_URL")}/page_news.php'><div>Новости</div></a></td>
                <li><a href='{$constant("APP_PHP_URL")}/debug.php'><div>Debug</div></a></td>
                <li><a href='{$constant("APP_PHP_URL")}/page_upload_user.php'><div>Загрузка песни</div></a></td>
                <li><a href='{$constant("APP_PHP_URL")}/page_add_info_news.php'><div>Загрузка новости</div></a></td>
                <li><a href='{$constant("APP_PHP_URL")}/page_add_info_author.php'><div>Загрузка автора</div></a></td>
                <li><a href='{$constant("APP_PHP_URL")}/page_delete.php'><div>Удаление</div></a></td>
            </ul>
        </div>
ABC;
        return $output;

    }

    // основное содержание страницы

    protected function Content()
    {

    }

    public function AppendMenu($menu)
    {
        $this->MenuPublic = $this->MenuPublic . $menu;
    }

    public function AppendContent($content)
    {
        $this->ContentPublic = $this->ContentPublic . $content;
    }

    public function OutputHtml()
    {
        $defaultHtml = $this->GetDefaultHtml();
        $output = sprintf($defaultHtml, $this->ScriptPublic, $this->CssPublic, $this->MenuPublic, $this->ContentPublic);

        print $output;
    }

    public function GetDefaultHtml()
    {
        $constant = 'constant';
        $defaultHtml = "
<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\"/>
        <script src=\"{$constant("APP_ASSETS_SCRIPTS_URL")}/js/jquery/jquery.min.js\" type=\"text/javascript\"></script>
        <script src=\"{$constant("APP_ASSETS_SCRIPTS_URL")}/css/bootstrap/js/bootstrap.js\" type=\"text/javascript\"></script>
        <link rel=\"stylesheet\" href=\"{$constant("APP_ASSETS_CSS_URL")}/css/bootstrap/css/bootstrap.css\"/> 
        <link rel=\"stylesheet\" href=\"{$constant("APP_CSS_URL")}/main.css\"/> 
        <script type=\"text/javascript\">
            var APP_PHP_URL = \"{$constant("APP_PHP_URL")}\";
            var APP_SCRIPTS_URL = \"{$constant("APP_SCRIPTS_URL")}\";
            var APP_MEDIA = \"{$constant("APP_MEDIA")}\";
            var APP_URL_DISPATCHER = APP_PHP_URL + \"/dispatcher.php?command=\";
        </script>
        %s
        %s
     </head>
     <body>
        
        <div class=\"container\">
        %s
            <div class=\"right-side-cont\">
                %s
            </div>
        </div>
    </body>
</html>
        ";
        return $defaultHtml;
    }

    public function AppendScript($script)
    {
        $scriptOutput = "<script src=\"" . APP_SCRIPTS_URL . "/{$script}.js\"></script>";

        $this->ScriptPublic = $this->ScriptPublic . $scriptOutput;
    }

    public function AppendAssetScript($script)
    {
        $scriptOutput = "<script src=\"" . APP_ASSETS_SCRIPTS_URL . "/{$script}.js\"></script>";

        $this->ScriptPublic = $this->ScriptPublic . $scriptOutput;
    }

    public function AppendCss($css)
    {
        $cssOutput = "<link rel=\"stylesheet\" href=\"" . APP_SCRIPTS_URL . "/{$css}.css\"/>";

        $this->CssPublic = $this->CssPublic . $cssOutput;
    }

    public function AppendAssetCss($css)
    {
        $cssOutput = "<link rel=\"stylesheet\" href=\"" . APP_ASSETS_CSS_URL . "/{$css}.css\"/>";

        $this->CssPublic = $this->CssPublic . $cssOutput;
    }
}

?>