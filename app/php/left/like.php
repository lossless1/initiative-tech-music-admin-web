<?php

require_once('dataprocessing.php');

class Likes extends dataprocessing
{
    public function Likes()
    {
        if (!isset($_GET) || !isset($_GET["song_id"])) {
            die('stop');
        }
        $this->ConnectDB();

        $user = R::findOne('users', 'user_login = ?', [$_SESSION['username']]);

        $iduser = $user['id'];
        $idsong = $_GET['song_id'];


        $likes = R::findOne("likes","id_user = ? AND id_song = ?", [$iduser,$idsong]);

        if (!$likes) {
            $likes = R::dispense('likes');

            $likes["idUser"] = $iduser;
            $likes["idSong"] = $idsong;
            echo 1;
            R::store($likes);
        } else {
            R::trash($likes);
            echo 0;
        }
    }
}
$start = new Likes();

?>