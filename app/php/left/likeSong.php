<?php

require_once('../dataprocessing.php');

class Likes extends dataprocessing
{
    public function Likes()
    {

        if (!isset($_GET) || !isset($_GET["song_id"])) {
            die('stop');
        }
        $this->ConnectDB();

        $user = R::findOne('users', 'user_login = ?', [$_SESSION['username']]);

        $iduser = $user['id'];
        $idsong = $_GET['song_id'];

        $likesIds = R::findOne("likessongs","id_user = ? AND id_song = ?", [$iduser,$idsong]);

        if ($likesIds) {
            $likes = R::dispense('likessongs');
            echo json_encode('1');
            $likes["idUser"] = $iduser;
            $likes["idSong"] = $idsong;

            R::store($likes);
        } else {
            echo json_encode('0');
            R::trash($likesIds);
        }
    }
}
$start = new Likes();

?>