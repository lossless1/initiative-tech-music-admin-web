<?php

require_once("dataprocessing.php");

class Search extends dataprocessing
{
    public function Search()
    {
        $this->ConnectDB();

        R::dispense('testmusic');
        $word = '%' . $_GET['name'] . '%';
        $search = R::findAll('testmusic', ' filename LIKE ? ', [$word]);

        $colMas = 0;
        if ($_GET['name'] == '*') {

            $word = '%' . $_GET['name'] . '%';
            $search = R::findAll('testmusic', ' filename LIKE ? ', [$word]);
            $this->ShowSongs($search, $colMas);
        }
        if ($_GET['name'] == '') {
            echo json_encode("Input song name!");
        }
        if (!$search && !empty($_GET['name'])) {
            echo json_encode("Song don't find!");
            //print_r($json);
        }
        if($search) {
            $this->ShowSongs($search, $colMas);
        }
    }

    public function ShowSongs($search, $colMas)
    {
        foreach ($search as $value) {
            $colMas++;

            $pathFile = MUSIC_UPLOAD . $value['filename'];

            $arr = array(
                'id' => $value['id'],
                'name' => $value['orgname'],
                'source' => $pathFile,
                'description' => $value['description'],
                'author' => $value['author'],
                'likes' => $value['likes'],
                'fullname' => $value['filename']);
            //$arr = str_replace("_"," ",$arr);
            $jsonMas[$colMas] = $arr;
        }

        @$json = json_encode($jsonMas, JSON_FORCE_OBJECT);
        echo $json;
    }
}

$search = new Search();
?>

