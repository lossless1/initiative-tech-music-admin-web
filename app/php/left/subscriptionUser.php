<?php
require_once('../dataprocessing.php');

class Subscriptions extends dataprocessing
{
    public function Subscriptions()
    {
        $this->ConnectDB();
        $subs = R::dispense('subscriptions');
        $userid = R::findOne('users', 'user_login = ?', [$_SESSION['username']]);

        $checksub = R::findOne('subscriptions', 'author = ? AND userid = ?', [$_GET['author'], $userid['id']]);

        //var_dump($checksub);

        if (!$checksub) {
            $subs['user_id'] = $userid['id'];
            $subs['author'] = $_GET['author'];
            R::store($subs);
            echo 'Подписано';
        } else {

            R::trash($checksub);
            echo 'Отписано';
        }


    }

    public function BeanToArray($bean)
    {
        $data = array();
        foreach ($bean as $beanKey => $beanValue) {

            $data[$beanKey] = $beanValue;
        }
        return $data;
    }

    public function BeansToArray($beans)
    {
        $result = array();
        foreach ($beans as $beansKey => $beansValue) {
            $data = $this->BeanToArray($beansValue);
            $result[] = $data;
        }

        return $result;
    }

}

$subscr = new Subscriptions();


?>

