<?php

require_once('../dataprocessing.php');

class Likes extends dataprocessing
{
    public function Likes()
    {
        if (!isset($_GET) || !isset($_GET["song_id"])) {
            die('stop');
        }
        $this->ConnectDB();

        $user = R::findOne('users', 'user_login = ?', [$_SESSION['username']]);

        $iduser = $user['id'];
        $idnews = $_GET['news_id'];

        $likesIds = R::findOne("likesnews", "user_id = ? AND news_id = ?", [$iduser, $idnews]);

        if ($likesIds) {
            $likes = R::dispense('likesnews');
            echo json_encode('1');
            $likes["user_id"] = $iduser;
            $likes["news_id"] = $idnews;

            R::store($likes);
        } else {
            echo json_encode('0');
            R::trash($likesIds);
        }
    }
}

$start = new Likes();

?>