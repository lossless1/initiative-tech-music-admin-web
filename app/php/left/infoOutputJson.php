<?php
require_once("../dataprocessing.php");

class android extends dataprocessing
{
    public function android()
    {
        $this->ConnectDB();
        if ($_GET['json']=='news') {
            echo $this->news();
        }
        if ($_GET['json']=='tags') {
            echo $this->tags();
        }
        if ($_GET['json']=='authors') {
           echo $this->author();
        }
        if ($_GET['json']=='users') {
            echo $this->users();
        }
        if (!$_GET){
            die("No command");
        }
    }

    public function news()
    {
        $news = R::getAll("SELECT * FROM news");
        return json_encode($news);
    }

    public function tags()
    {
        $tags = R::getAll("SELECT * FROM tags");
        return json_encode($tags);
    }

    public function author()
    {
        $authors = R::getAll("SELECT * FROM author");
        return json_encode($authors);
    }

    public function users()
    {
        $users = R::getAll("SELECT * FROM users");
        return json_encode($users);


    }
}

$android = new android();
?>