<?php

require_once('dataprocessing.php');

class Upload extends dataprocessing
{
    public $array, $uploadDir, $tmpName, $pathFile, $orgname, $filename, $dateTime, $comment, $author, $songname, $md5;

    public function Upload()
    {
        //echo '<pre>';
        //var_dump($_FILES);
        //var_dump($_POST["comment"]);
        if(CheckFiles('userfile')){
            $array = array("_(zaycev.net)", ".mp3", ".mp4", ".mpeg");
            $uploadDir = MUSIC_UPLOAD;
            $tmpName = $_FILES['userfile']['tmp_name'];
            $pathFile = $uploadDir . $_FILES['userfile']['name'];
            $orgname = str_replace($array, "", basename($_FILES['userfile']['name']));
            $filename = $_FILES['userfile']['name'];
            $dateTime = $this->DateYMD();
            $comment = $_POST["comment"];

            $author = $this->Author($orgname);
            $songname = $this->SongName($orgname);


            $md5 = md5_file($tmpName);

            if (empty($_FILES)) {
                echo "Передайте файлы!!.<br>";
            } else {
                $this->UploadMusic($filename, $comment, $author, $dateTime, $tmpName, $pathFile, $orgname, $md5, $songname);
            }
        }

    }

    public function CreateDatabase($filename, $description, $author, $date, $orgname, $md5, $songname)
    {
        $musics = R::dispense('testmusic');
        $musics["filename"] = $filename;
        $musics["description"] = $description;
        $musics["author"] = $author;
        $musics["songname"] = $songname;
        $musics["date"] = $date;
        $musics["orgname"] = $orgname;
        $musics["md5"] = $md5;
        $musics["likes"] = 0;
        R::store($musics);
    }

    public function UploadMusic($filename, $comment, $author, $dateTime, $tmpName, $pathFile, $orgname, $md5, $songname)
    {
        $this->ConnectDB();

        $md5Music = R::getRow('SELECT * FROM testmusic WHERE md5 LIKE ? LIMIT 1', [$md5]);

        if ($md5 == $md5Music['md5']) {
            echo "Такая песня уже есть! Загрузите что-то другое!";
            exit;
        } else {
            $this->CreateDatabase();
            if (move_uploaded_file($tmpName, $pathFile)) {
                echo "UPLOAD_SUCCESS\n";
            } else {
                echo "UPLOAD_FAIL\n";
            }
            echo 'Here is some more debugging info:<br><br>';

            echo " Успешно загружен " . $author . "-" . $songname;
            echo "<br>";
            echo '<pre>';
        }

    }

    public function Author($orgname)
    {
        $arrayAuthor = explode("-", $orgname);
        $arrayAuthor = str_replace("_", " ", $arrayAuthor);
        $stringAuthor = ucwords($arrayAuthor[0]);
        return $stringAuthor;
    }

    public function SongName($orgname)
    {
        $arraySongName = explode("-", $orgname);
        $arraySongName = str_replace("_", " ", $arraySongName);
        return $arraySongName[1];
    }
}

$upload = new Upload();

?>
