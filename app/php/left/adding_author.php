<?php

require_once('dataprocessing.php');

class addingAuthor extends dataprocessing
{
    public $authorName, $avatar;

    public function addingAuthor()
    {
        global $authorName, $avatarUrl;
        $this->ConnectDB();

        $authorName = $_POST['authorname'];
        $avatarUrl = IMAGE_UPLOAD . $_FILES['picture']['name'];


        $authors = R::findOne('author', 'author_name = ?', [$authorName]);
        if (!$authors) {
            $this->InsertAuthors();
            move_uploaded_file($_FILES['picture']['tmp_name'], $avatarUrl);
            echo "Автор успешно загружен!";
        } else {
            echo "Такой автор уже есть";
        }
    }

    public function InsertAuthors()
    {
        global $authorName, $avatarUrl;
        $author = R::dispense('author');
        $author['author_name'] = $authorName;
        $author['avatar'] = $avatarUrl;
        $author['quantity_subs'] = $this->QuantitySubs();
        R::store($author);
    }

    public function QuantitySubs()
    {
        global $authorName;
        $id = R::getRow("SELECT * FROM author WHERE author_name = ?", [$authorName]);
        @$subs = R::findAll("subscriptions", "author = ?", [$id['id']]);
        if (!$subs) {
            //echo "0";
            return 0;

        } else {
            //echo "1";
            return count($subs);
        }
    }
}

$author = new addingAuthor();