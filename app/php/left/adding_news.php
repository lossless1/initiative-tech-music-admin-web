<?php
require_once('dataprocessing.php');

class addingNews extends dataprocessing
{
    public $idAuthor, $idTags, $linkSong, $picture, $title, $likes;

    public function addingNews()
    {
        global $idAuthor, $idTags, $picture, $title, $linkSong;
        $this->ConnectDB();
        $idAuthor = $_POST['idauthor'];
        $idTags = $_POST['idtags'];
        $linkSong = MUSIC_UPLOAD . $_FILES['song']['name'];
        $picture = AVATAR_IMAGE_UPLOAD . $_FILES['picture']['name'];
        $title = $_POST['title'];

        if (empty($_FILES['pictures']['name']) && empty($_FILES['song']['name'])) {
            echo "Что-то пошло не так!";
            echo "Не выбрали файлы!";
            //var_dump($_FILES);
        } else {
            echo "<pre>";
            var_dump($picture);
            move_uploaded_file($_FILES['picture']['tmp_name'], $picture);
            move_uploaded_file($_FILES['song']['tmp_name'], $linkSong);
            echo "Новость успешно загружена!";
        }
    }

    public function InputTableNews()
    {
        global $picture, $linkSong;
        $news = R::dispense('news');
        $news['idauthor'] = $this->InputAuthor(); //page
        $news['idtags'] = $this->InputTags(); //page
        $news['linksong'] = $linkSong; //page
        $news['likes'] = $this->LikesNews();
        $news['additionTime'] = $this->DateYMD();
        $news['pictureNews'] = $picture; //page
        $news['title'] = $_POST['title']; //page
        R::store($news);
    }

    public function InputTags()
    {
        $tagid = R::getRow("SELECT * FROM tags WHERE name_tag = ?",[$_POST['idtags']]);
        if(!$tagid){
            $tagsDb = R::dispense('tags');
            $tagsDb['name_tag'] = $_POST['idtags'];
            R::store($tagsDb);
        }
        return $_POST['idtags'];
    }

    public function LikesNews()
    {
        global $likes;
        $id = R::getRow("SELECT * FROM news WHERE idauthor = ?", [$_POST['idauthor']]);
        $likes = R::findAll("likesnews", "id_news = ?", [$id['id']]);
        return count($likes);
    }

    public function InputAuthor(){
        return $_POST['idauthor'];
    }
}

$news = new addingNews();


?>