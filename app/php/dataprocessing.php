<?php
set_time_limit(0);
session_start();

$unixPath = "/Users/lossless/Documents/Projects/localsites/music-web";
$linuxPath = "/var/www/music-web";
if (file_exists($unixPath)) {
    define("APP_DIR", $unixPath);
} else if (file_exists($linuxPath)) {
    define("APP_DIR", $linuxPath);
}

define("APP_PHP_DIR", APP_DIR . "/app/php");
define("APP_ASSETS_PHP_DIR", APP_DIR . "/app/assets/php");
define("APP_PHP_USER_DIR", APP_DIR . "/app/php/user/");
define("MUSIC_UPLOAD", APP_DIR . "/app/assets/media/");
define("IMAGE_UPLOAD", APP_DIR . "/app/assets/images/");
define("AVATAR_IMAGE_UPLOAD", APP_DIR . "/app/assets/images/avatar/");


define("APP_HOST", "http://{$_SERVER["HTTP_HOST"]}");
define("APP_URL", APP_HOST . "/music-web");
define("APP_PHP_URL", APP_URL . "/app/php");
define("APP_SCRIPTS_URL", APP_URL . "/app/scripts");
define("APP_ASSETS_SCRIPTS_URL", APP_URL . "/app/assets");
define("APP_CSS_URL", APP_URL . "/app/styles");
define("APP_ASSETS_CSS_URL", APP_URL . "/app/assets");
define("APP_PHP_USER_URL", APP_URL . "/app/php/user");
define("APP_MEDIA", APP_URL . "/app/assets/media");
define("APP_IMAGE_URL", APP_URL . "/app/assets/images");
define("APP_VIEW_URL", APP_URL . "/view");

global $DB;
$DB['host'] = 'localhost';
$DB['dbname'] = 'musicdb';
$DB['user'] = 'lossless';
$DB['password'] = 'kalifornia2904';

define("DB_PREFIX", "audio_");
require_once(APP_ASSETS_PHP_DIR . "/redbeans/rb.php");


//класс для обработки данных, этот класс является родителем всех остальных
class dataprocessing
{
    protected $ContentPublic;
    protected $ScriptPublic;
    protected $CssPublic;
    protected $MenuPublic;
    protected $ScriptUser;
    protected $ContentUser;
    protected $DefaultMenu;

    public function ConnectDB()
    {
        global $DB;
        R::setup('mysql:host=' . $DB['host'] . '; dbname=' . $DB['dbname'], $DB['user'], $DB['password']);
    }

    public function CheckUserNumber($number)
    {
        // составляем шаблон
        $patt = '[[:alpha:]]|[[:punct:]]|[[:cntrl:]]|[[:space:]]';
        // или вот так $patt = '[^0-9]';
        //меняем на '', т.е. на пустое место
        $replace = '';
        return preg_replace($patt, $replace, $number);
    }

    public function CheckTable($table)
    {
        $uer = R::exec("SHOW TABLES LIKE ?", $table);
        if (!$uer) {
            $user = R::exec('create table users
            (
            id integer not null auto_increment primary key,
            user_login varchar(20),
            user_passwd varchar(40),
            user_email varchar(35),
            user_name varchar(60),
            user_city varchar(30),
            user_phone varchar(11)
            )');
            R::store($user);
        }
    }

    public function DecodeSymbols($infoUser)
    {
        $s = preg_replace('/\\\u0([0-9a-fA-F]{3})/', '&#x\1;', $infoUser);
        $symbols = html_entity_decode($s, ENT_NOQUOTES, 'UTF-8');//документ должен быть в кодировке utf-8
        return $symbols;
    }

    public function DateYMD()
    {
        $date = time(); // c в секундах 1 января 1970 00:00:00 GMT

        return $date;
    }

    public function CheckGet($key, $canBeNull = false)
    {
        $result = $this->FilterDataByMethod("get", $key, $canBeNull);
        return $result;
    }

    public function FilterDataByMethod($method, $key, $canBeNull)
    {
        $lowerMethod = strtolower($method);
        if ($lowerMethod === "get") {
            $methodVariable = &$_GET;
        } else if ($lowerMethod === "post") {
            $methodVariable = &$_POST;
        } else if ($lowerMethod === "session") {
            $methodVariable = &$_SESSION;
        } else if ($lowerMethod === "cookie") {
            $methodVariable = &$_COOKIE;
        } else if ($lowerMethod === "files") {
            $methodVariable = &$_FILES;
        } else {
            return null;
        }

        $invalid = false;
        if (!isset($methodVariable) || !isset($methodVariable[$key])) {
            $invalid = true;
        }
        if ($invalid && $canBeNull) {
            return null;
        } else if ($invalid && !$canBeNull) {
            OutputFailure(array(), "{$method} with key:{$key} not found");
        }

        return $methodVariable[$key];
    }

    public function CheckPost($key, $canBeNull = false)
    {
        $result = $this->FilterDataByMethod("post", $key, $canBeNull);
        return $result;
    }

    public function CheckSession($key, $canBeNull = false)
    {
        $result = $this->FilterDataByMethod("session", $key, $canBeNull);
        return $result;
    }

    public function CheckCookie($key, $canBeNull = false)
    {
        $result = $this->FilterDataByMethod("cookie", $key, $canBeNull);
        return $result;
    }

    public function CheckFiles($key, $canBeNull = false)
    {
        $result = $this->FilterDataByMethod("files", $key, $canBeNull);
        return $result;
    }

    public function BeansToArray($beans)
    {
        $result = array();
        foreach ($beans as $beansKey => $beansValue) {
            $data = $this->BeanToArray($beansValue);
            $result[] = $data;
        }

        return $result;
    }

    public function BeanToArray($bean)
    {
        $data = array();
        foreach ($bean as $beanKey => $beanValue) {

            $data[$beanKey] = $beanValue;
        }
        return $data;
    }

    public function OutputSuccess($data,$message = '', $result = 'SUCCESS')
    {
        $outputArray = array();

        $outputArray['data'] = $data;
        $outputArray["message"] = $message;
        $outputArray["result"] = $result;

        die(json_encode($outputArray));
    }

    public function OutputFailure($data,$message = '', $result = 'FAILURE')
    {
        $outputArray = array();

        $outputArray['data'] = $data;
        $outputArray["message"] = $message;
        $outputArray["result"] = $result;

        die(json_encode($outputArray));
    }
}

?>