<?php
require_once('dataprocessing.php');

class page_user extends dataprocessing
{

    public function DisplayPage()
    {

        $content = $this->Content();

        $this->AppendContent($content);
        $this->OutputHtml();

    }

    protected function Content()
    {

    }

    public function AppendContent($content)
    {
        $this->ContentUser = $this->ContentUser . $content;
    }

    public function OutputHtml()
    {
        $defaultMenu = $this->DefaultMenu();
        $output = sprintf($defaultMenu, $this->ScriptUser, $this->ContentUser);
        print $output;
    }

    private function DefaultMenu()
    {
        $constant = 'constant';
        if (isset($_SESSION['username'])) {
            $menu = "
<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\"/>
        <script src=\"{$constant("APP_ASSETS_SCRIPTS_URL")}/js/jquery/jquery.min.js\" type=\"text/javascript\"></script>
        <script src=\"{$constant("APP_ASSETS_CSS_URL")}/css/bootstrap/js/bootstrap.js\" type=\"text/javascript\"></script>
        <link rel=\"stylesheet\" href=\"{$constant("APP_ASSETS_CSS_URL")}/css/bootstrap/css/bootstrap.css\"/> 
        <link rel=\"stylesheet\" href=\"{$constant("APP_CSS_URL")}/main.css\"/> 
        <script type=\"text/javascript\">
            var APP_PHP_URL = \"{$constant("APP_PHP_URL")}\";
            var APP_SCRIPTS_URL = \"{$constant("APP_SCRIPTS_URL")}\";
            var APP_MEDIA = \"{$constant("APP_MEDIA")}\";
            var APP_URL_DISPATCHER = APP_PHP_URL + \"/dispatcher.php?command=\";
        </script>
    %s
    </head>
    <body>
    <div class='container'>
        <div class=\"menu\">
            <ul>
                <li>
                    <a href=\"{$constant('APP_PHP_URL')}/page_search_public.php\">
                        <div>
                            Вернуться на главную страницу
                        </div>
                    </a>
                </li>
                <li>
                    <a href=\"{$constant('APP_PHP_URL')}/dispatcher.php?command=logout\">
                        <div>
                            Выйти со своей страницы
                        </div>
                        
                    </a>
                </li>
            </ul>
             %s
    </div>
        
       
    </body>
</html>
        ";
        } else {
            $menu = "
            <html>
            <head>
            
            </head>
            <body>
                You are not login in
            </body>
            </html>
            ";
        }
        return $menu;
    }

    public function AppendScript($script)
    {
        $scriptOutput = "<script src=\"" . APP_SCRIPTS_URL . "/{$script}.js\"></script>";

        $this->ScriptPublic = $this->ScriptPublic . $scriptOutput;
    }

    public function AppendAssetScript($script)
    {
        $scriptOutput = "<script src=\"" . APP_ASSETS_SCRIPTS_URL . "/{$script}.js\"></script>";

        $this->ScriptPublic = $this->ScriptPublic . $scriptOutput;
    }

    public function AppendCss($css)
    {
        $cssOutput = "<link rel=\"stylesheet\" href=\"" . APP_SCRIPTS_URL . "/{$css}.css\"/>";

        $this->CssPublic = $this->CssPublic . $cssOutput;
    }

    public function AppendAssetCss($css)
    {
        $cssOutput = "<link rel=\"stylesheet\" href=\"" . APP_ASSETS_CSS_URL . "/{$css}.css\"/>";

        $this->CssPublic = $this->CssPublic . $cssOutput;
    }
}

?>