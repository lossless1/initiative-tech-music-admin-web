<?php

require_once ("page_public.php");

class debug extends page_public{

    public function Content(){

        $this->AppendScript("debug");
        $debugView = <<<DBG
        <div class="right-side-cont">
        <div id="info"></div>
        </div>
DBG;
        return $debugView;
    }
}

$debug = new debug();
$debug->DisplayPage();


