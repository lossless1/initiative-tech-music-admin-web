<?php
require_once('page_public.php');

class registration_form extends page_public
{
    protected function Content()
    {
        $this->AppendScript("reglog");
        $register = "        
        <div>Registration:</div>
        <div id=\"divRegister\">
            <div class='form-group-sm col-xs-3'>
                    <label for='usr'>Username:</label>
                    <input type=\"text\" class=\"form-control\" id=\"username_register\" autocomplete=\"off\"/>                
                    <span id=\"error_login\" style=\"color:red\"></span>
                    <br>
                    <label for='pass'>Password:</label>
                    <input type=\"password\" class=\"form-control \" id=\"password_register\" autocomplete=\"off\"/>
                    <span id=\"error_password\" style=\"color:red\"></span>
                    <br>
            
            
            <label for='repass'>Re-enter password:</label>
            <input type=\"password\" class=\"form-control\"id=\"reenter_password_register\" autocomplete=\"off\"/>
            <span id=\"error_repassword\" style=\"color:red\"></span><br>
            <span id=\"error_match\" style=\"color:red\"></span>
            <br>
            <label for='email'>Email:</label>
            <input type=\"email\" id=\"email_register\" class=\"form-control\" autocomplete=\"off\"/><div id=\"error_email\" style=\"color:red\"></div><br/>
            <button id=\"register\" class='btn btn-default'>Register</button><br />
            <div align=\"center\" id=\"infoReg\"></div>
            </div>
        </div>
        ";
        return $register;
    }
}

$page = new registration_form();
$page->DisplayPage();
?>