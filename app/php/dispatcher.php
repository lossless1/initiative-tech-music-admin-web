<?php
require_once("./dataprocessing.php");
require_once("./mp3File/mp3File.php");

class Dispatcher extends dataprocessing
{

    public $username = null;


//VIEW NEWS //OK
    public $password = null;

//VIEW NEWS WITH IDS
    public $email = null;

//VIEW NEWS WITH IDAUTHOR
    public $defaultAvatarUrl = null;

//VIEW NEWS WITH IDS AND IDAUTHORS
    public $authorName = null;

//VIEW TAGS NEWS //OK
    public $avatarUrl = null;

//VIEW TAGS STORY //OK
    public $pictureUrl = null;

//VIEW AUTHORS //OK
    public $linkSongUrl = null;

//VIEW USERS //OK
    public $likes = null;

//VIEW USER SUBSCRIBES //OK
    public $array = null;
    public $uploadDir = null;
    public $fullFilePath = null;
    public $originalName = null;
    public $fileName = null;

//DELETE TABLE
    public $authorDb = null;
    public $songName = null;
    public $md5File = null;
    public $key = null;

    public function __construct()
    {
        $this->ConnectDB();
        if (empty($_GET) && empty($_POST) && empty($_FILES)) {
            $this->OutputFailure("No command");
        }

        @$session = $_SESSION['username'];
        $command = $_GET['command'];

        if (empty($_GET['language'])) {
            $_GET['language'] = '';
        }
        if ($command == 'get_users') {
            $this->ShowUser();
        } //OK
        if ($command == 'get_authors') {
            $this->ShowAuthor();
        } //OK
        if ($command == 'get_tags') {
            $this->ShowTags();
        } //OK
        if ($command == 'get_story') {
            $this->ShowStory();
        }
        if ($command == 'get_news') {
            if (isset($_POST['ids']) && isset($_POST['idauthor'])) {
                $this->ShowNewsByIdsAndIdAuthor();
            } elseif (isset($_POST['idauthor'])) {
                $this->ShowNewsByIdAuthor();
            } elseif (isset($_POST['ids'])) {
                $this->ShowNewsByIds();
            } else {
                $this->ShowNews();
            }
        } //OK
        if ($command == 'get_categ') {
            $this->ShowCategories();
        }
        if ($command == 'get_songs') {
            $this->ShowSong();
        }
        if ($command == 'get_users_subscribers') {
            $this->ShowUserSubscription();
        }
        if ($command == 'get_session') {
            $this->UserSession();
        }
        if ($command == 'get_author_song_data') {
            $this->GetSongDataByAuthorName();
        }
        if ($command == 'get_news_by_tag_news') {
            $this->GetNewsByTagNews();
        }
        if ($command == 'get_user_data') {
            $this->ShowUserDataById();
        }
        if ($command == "get_tables") {
            $this->ShowTables();
        }
        if ($command == "get_information_from_table") {
            $this->ShowInfoTable();
        }

        if ($command == 'authentication') {
            $this->AuthorizationUser();
        }//OK
        if ($command == 'logout') {
            $this->LogOut();
        }
        if ($command == 'delete_user') {
            $this->DeleteUser();
        }
        if ($command == 'user_registration') {
            $this->Registration();
        }//OK

        if ($command == 'adding_author') {
            $this->AddingAuthor();
        }//OK
        if ($command == 'adding_news') {
            $this->AddingNews();
        }//OK
        if ($command == 'adding_story') {
            $this->AddingStory();
        }
        if ($command == 'adding_tags_to_user') {
            $this->AddingTagsToUser();
        }//OK
        if ($command == 'delete_news_by_id_news') {
            $this->DeleteNews();
        }
        if ($command == 'add_to_playlist') {
            $this->AddToPlaylist();
        }//ok
        if ($command == 'search_music') {
            $this->SearchMusic();
        } //ok
        if ($command == 'upload_music') {
            $this->UploadMusic();
        }
        if ($command == 'update_user') {
            $this->UpdateUser();
        }
        if ($command == 'like_song') {
            $this->LikeSong();
        }
        if ($command == 'like_news') {
            $this->AddLikeNewsByIdNews();
        }//WORK GOOD OK

//GET LIKE FOR PAGE_SONG.JS
        if ($command == 'songslikes') {
            $this->GetLikesForOneSong();
        }//POST

//DELETING TABLE
        if ($command == 'delete_table') {
            $this->DeleteTable();
        }

        if ($command == "valid_username") {
            $this->OutputSuccess(array(), $this->ValidUsername());
        }
        if ($command == "valid_password") {
            $this->OutputSuccess(array(), $this->ValidPassword());
        }
        if ($command == "valid_repassword") {
            $this->OutputSuccess(array(), $this->ValidPassword());
        }
        if ($command == "valid_email") {
            $this->OutputSuccess(array(), $this->ValidEmail());
        }
        if ($command == "match_passwords") {
            $this->OutputSuccess(array(), $this->MatchPasswords());
        }
    }

//REGISTRATION USERS //OK

    public function ShowUser()
    {
        $users = R::getRow("SELECT * FROM users WHERE user_login = ?", [$_SESSION['username']]);
        $this->OutputSuccess($users);
    }

    public function ShowAuthor()
    {
        $authors = R::getAll("SELECT * FROM author");
        $this->OutputSuccess($authors);
    }

//проверка

    public function ShowTags()
    {
        $tags = R::getAll("SELECT * FROM tags");
        $this->OutputSuccess($tags);
    }

    public function ShowStory()
    {
        $tags = R::getAll("SELECT * FROM news WHERE category = 'Story'");
        $this->OutputSuccess($tags);
    }

    public function ShowNewsByIdsAndIdAuthor()
    {
        $arrayNewsIds = explode(",", $this->CheckPost('ids'));
        $arrayAuthorIds = explode(",", $this->CheckPost('idauthor'));

        $arrayNews = [];
        foreach ($arrayNewsIds as $value) {
            $news = R::getRow("SELECT * FROM news WHERE id = ?", [$value]);
            foreach ($arrayAuthorIds as $valueAuthors) {

                if ($valueAuthors == $news['idauthor']) {
                    if ($news != null) {
                        $arrayNews[] = $news;
                    }
                }
            }
        }

        $this->OutputSuccess($arrayNews);
    }

//ADD AUTHORS //ok

    public function ShowNewsByIdAuthor()
    {
        $news = R::getAll("SELECT * FROM news WHERE idauthor = ?", [$this->CheckPost('idauthor')]);
        $this->OutputSuccess($news);
    }

    public function ShowNewsByIds()
    {
        $arrayNewsIds = explode(",", $this->CheckPost('ids'));
        $arrayNews = [];
        foreach ($arrayNewsIds as $value) {
            $news = R::getRow("SELECT * FROM news WHERE id = ?", [$value]);
            if ($news != null) {
                $arrayNews[] = $news;
            }
        }
        $this->OutputSuccess($arrayNews);
    }

    public function ShowNews()
    {
        $news = R::getAll("SELECT * FROM news ORDER BY addition_time LIMIT 100");
        $this->OutputSuccess($news);
    }

    public function ShowCategories()
    {
        $this->OutputSuccess(["News", "Articles", "Story"]);
    }

    public function ShowSong()
    {
        $song = R::getAll("SELECT * FROM music");
        $this->OutputSuccess($song);
    }


//ADD NEWS

    public function ShowUserSubscription()
    {
        $userId = R::getRow("SELECT * FROM users WHERE user_login = ?", [$_SESSION['username']]);
        $sub = R::getAll("SELECT * FROM subscriptions WHERE userid = ?", [$userId['id']]);
        $this->OutputSuccess($sub);
    }

    public function UserSession()
    {
        global $session;
        $session = $_SESSION['username'];
        $this->OutputSuccess($session);
    }

    public function GetSongDataByAuthorName()
    {
        $author = $_POST['author'];
        $dataSongDb = R::getAll('SELECT * FROM music WHERE author = ?', [$author]);
        $this->OutputSuccess($dataSongDb);
    }

    public function GetNewsByTagNews()
    {
        $idTagsNews = $_POST['idtags'];

        $arrayIdTags = explode(",", $idTagsNews);

        $tag = [];
        $dataNewsDb = R::getAll('SELECT * FROM news');
        foreach ($dataNewsDb as $valueDb) {
            foreach ($arrayIdTags as $valueTags) {
                $arrayIdTagsDb = explode(",", $valueDb['idtags']);
                foreach ($arrayIdTagsDb as $valueDbTags) {

                    if ($valueTags == $valueDbTags) {
                        $tag[] = $valueDb;
                    }
                }
            }
        }
        $this->OutputSuccess($tag);
    }

    public function ShowUserDataById()
    {
        $userDataEmail = R::getAll("SELECT * FROM users WHERE user_email = ?", [$_SESSION['username']]);
        return $userDataEmail;
    }

    public function ShowTables()
    {
        $tables = R::inspect();
        $this->OutputSuccess($tables);
    }

    public function ShowInfoTable()
    {
        $post = $this->CheckPost("table");
        $col = R::getAll("SELECT * FROM $post");
        $this->OutputSuccess($col);
    }

    public function AuthorizationUser()
    {
        $email = $this->CheckUserData($_POST['user_email']);
        $password = $this->CheckUserData($_POST['user_password']);

        $result = R::findOne("users", "user_email = ?", [$email]);
        $hash = $result['user_password'];

        $errorLogin = "Email and password are invalid.";
        $errorEmptyTextBoxes = "Textboxes are empty. Please input Email/password.";
        $successLogin = "Congratz. You are in.";

        if ($this->CheckPost("language") == "en") {
            $errorLogin = "Email and password are invalid.";
            $errorEmptyTextBoxes = "Textboxes are empty. Please input Email/password.";
            $successLogin = "Congratz. You are in.";
        }
        if ($this->CheckPost("language") == "ru") {
            $errorLogin = "Email или/и пароль не верны.";
            $errorEmptyTextBoxes = "Не все поля заполнены. Введите пожалуйста Email или/и пароль.";
            $successLogin = "Поздравляю. Вы успешно зашли.";
        }

        if ($email && $password) {
            if (password_verify($password, $hash)) {
                $_SESSION['username'] = $email;
                setcookie('username', $_SESSION['username'], time() + 3600, '/');
                $this->OutputSuccess($this->ShowUserDataById($email), $successLogin);
            } else {
                $this->OutputFailure(array(), $errorLogin);
            }
        } else {
            $this->OutputFailure(array(), $errorEmptyTextBoxes);
        }
    }

    public function CheckUserData($var)
    {
        $res = htmlspecialchars($var, ENT_QUOTES);
        return addslashes($res);
    }

    public function LogOut()
    {
        if (isset($_SESSION['username'])) {
            unset($_SESSION['username']);
            setcookie('username', '', time() + 3600, '/');
            header("Location: " . APP_URL . "/index.php");
        }
    }

    public function DeleteUser()
    {
        $user = R::findOne('users', 'user_email = ?', [$_SESSION['username']]);
        if (!$user) {
            $user = R::findOne('users', 'user_idvk = ?', [$_SESSION['username']]);
        }
        if (!$user) {
            $user = R::findOne('users', 'user_facebookid = ?', [$_SESSION['username']]);
        }
        if (!$user) {
            $user = R::findOne('users', 'user_googleid = ?', [$_SESSION['username']]);
        }
        R::trash($user);
    }

//DELETE NEWS

    public function Registration()
    {


        global $data, $username, $password, $email, $defaultAvatarUrl;

        $errorEmptyTextboxes = "Some textboxes are empty.";
        $errorUsernameAlreadyHas = "This login already registered.";
        $errorUsernameValidSybmols = "Username must have min 6 chars and does not have !@#$%^&* chars etc.";
        $errorPassword = "Password must contain 8-20 char or letter.";
        $errorMatchPasswords = "Passwords don't coincide.";
        $errorEmailAlreadyHas = "Email fail.";
        $errorEmailValid = "This is not Email. -_-";
        $successRegistration = "User registered. ^_^";

        $username = $this->CheckUserData($this->CheckPost("username"));
        $password = $this->CheckUserData($this->CheckPost("password"));
        $repassword = $this->CheckUserData($this->CheckPost("repassword"));
        $email = $this->CheckUserData($this->CheckPost("email"));
        $defaultAvatarUrl = APP_IMAGE_URL . "/user_avatars/default-avatar.png";

        $data = R::findOne("users", "user_email = ?", [$email]);

        if ($this->CheckPost('language') == "en") {
            $errorEmptyTextboxes = "Some textboxes are empty.";
            $errorUsernameAlreadyHas = "This login already registered.";
            $errorUsernameValidSybmols = "Username must have min 6 chars and does not have !@#$%^&* chars etc.";
            $errorPassword = "Password must contain 8-20 char or letter.";
            $errorMatchPasswords = "Passwords don't coincide.";
            $errorEmailAlreadyHas = "This is not Email -_-";
            $errorEmailValid = "Email fail.";
            $successRegistration = "User registered. ^_^";
        }
        if ($this->CheckPost('language') == "ru") {
            $errorEmptyTextboxes = "Есть не заполненные поля!";
            $errorUsernameAlreadyHas = "Такой логин уже зарегистрирован.";
            $errorUsernameValidSybmols = "Логин должен иметь как минимум 6 символов и не иметь специальных символов!@#$%^&* и их подобные.";
            $errorPassword = "Пароль написан не правильно.";
            $errorRepassword = "Повторный пароль написан не правильно.";
            $errorMatchPasswords = "Пароли не совпадают.";
            $errorEmailAlreadyHas = "Это не email. -_-";
            $errorEmailValid = "Такой email уже существует, введите другой.";
            $successRegistration = "Поздравляю, вы зарегистрировались. ^_^";
        }

        if (empty($username) || empty($password) || empty($repassword) || empty($email)) {
            $this->OutputFailure(array(), $errorEmptyTextboxes);
        } else if ($this->FindLogin($username)) {
            $this->OutputFailure(array(), $errorUsernameAlreadyHas);
        } else if (!($this->ValidUsername() == 'Success')) {
            $this->OutputFailure(array(), $errorUsernameValidSybmols);
        } else if (!($this->ValidPassword() == 'Success')) {
            $this->OutputFailure(array(), $errorPassword);
        } else if (!($password == $repassword)) {
            $this->OutputFailure(array(), $errorMatchPasswords);
        } else if ($this->FindEmail($email)) {
            $this->OutputFailure(array(), $errorEmailAlreadyHas);
        } else if (!($this->ValidEmail() == 'Success')) {
            $this->OutputFailure(array(), $errorEmailValid);
        } else {
            $password = password_hash($password, PASSWORD_DEFAULT);
            $this->CreateUserDb();
            $this->OutputSuccess(array(), $successRegistration);
        }
    }


//ADD TAGS STORY

    public function FindLogin($login)
    {
        $result = R::findOne("users", "user_login = ?", [$login]);
        return $result;
    }

    public function ValidUsername()
    {
        $username = $_POST['username'];
        if (preg_match('/^(?=.*[a-zA-Z0-9]).{6,20}$/', $username)) {
            return "Success";
        } else {
            return "Username must have min 6 chars and does not have !@#$%^&* chars etc.";
        }
    }

    public function ValidPassword()
    {
        $bool = null;
        if (empty($_POST['repassword'])) {
            $bool = $_POST['password'];
        } else {
            $bool = $_POST['repassword'];
        }
        $bool = preg_match("/^(?=.*[a-zA-Z0-9]).{8,20}$/", $bool);

        if ($bool) {
            return "Success";
        } else {
            return "Fail! - must contain 8-20 char or letter";
        }
    }

//LIKE SONG //OK

    public function FindEmail($email)
    {
        $result = R::findOne("users", " user_email = ?", [$email]);
        return $result;
    }

//SEARCH MUSIC //OK

    public function ValidEmail($email = "")
    {
        if (empty($email)) {
            $email = $_POST['email'];
        }
        $bool = $email;
        $bool = preg_match("/[a-z0-9]+[_a-z0-9\.-]*[a-z0-9]+@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/", $bool);
        if ($bool) {
            return "Success";
        } else {
            return "Fail!";
        }
    }

    public function CreateUserDb()
    {
        global $username, $password, $email, $defaultAvatarUrl;
        $user = R::dispense("users");
        $user["user_login"] = $username;
        $user["user_password"] = $password;
        $user["user_email"] = $email;
        $user["avatar"] = $defaultAvatarUrl;
        $user["tags_ids"] = null;
        $user["news_added_ids"] = null;
        $user["news_liked_ids"] = null;
        R::store($user);
    }

//UPLOAD MUSIC FORMDATA WRONG

    public function AddingAuthor()
    {
        global $authorName, $avatarUrl;

        $authorName = $this->CheckPost('authorname');

        $authors = R::findOne('author', 'author_name = ?', [$authorName]);


        if ($authors) {
            $this->OutputFailure(array(), "Author already registered!");
        } elseif (empty($_POST['authorname'])) {
            $this->OutputFailure(array(), "Author name is not found!");
        } elseif (!$this->CheckFiles('avatar')) {
            $this->OutputFailure(array(), "Avatar is not found!");
        } else {
            $avatarUrl = APP_IMAGE_URL . '/authors_avatars/' . $_FILES['avatar']['name'];
            $avatarDir = IMAGE_UPLOAD . 'authors_avatars/' . $_FILES['avatar']['name'];
            move_uploaded_file($_FILES['avatar']['tmp_name'], $avatarDir);
            $this->InsertAuthors();
            $this->OutputSuccess(array(), "Author register success!");
        }
    }

    public function InsertAuthors()
    {
        global $authorName, $avatarUrl;
        $author = R::dispense('author');
        $author['author_name'] = $authorName;
        $author['avatar'] = $avatarUrl;
        $author['quantity_subs'] = $this->QuantitySubs();
        R::store($author);
    }

    public function QuantitySubs()
    {
        global $authorName;
        $id = R::getRow("SELECT * FROM author WHERE author_name = ?", [$authorName]);
        @$subs = R::findAll("subscriptions", "author = ?", [$id['id']]);
        if (!$subs) {
            return 0;
        } else {
            return count($subs);
        }
    }

    public function AddingNews()
    {
        global $pictureUrl;

        $this->$this->CheckPost('nameauthor');
        $this->$this->CheckPost('nametags');
        $this->$this->CheckPost('namestory');
        $this->$this->CheckPost('categories');
        $this->$this->CheckPost('song');
        $this->$this->CheckPost('content');
        $this->$this->CheckPost('title');

        if (empty($_FILES['avatar']['name'])) {
            $this->OutputFailure(array(), "File dont choose!");
        } elseif (empty($_POST['nameauthor'])) {
            $this->OutputFailure(array(), "Author is not selected ");
        } elseif (empty($_POST['nametags'])) {
            $this->OutputFailure(array(), "Tag is not selected");
        } elseif (empty($_POST['categories'])) {
            $this->OutputFailure(array(), "Category is not selected");
        } elseif (empty($_POST['title'])) {
            $this->OutputFailure(array(), "Title is not selected");
        } elseif (empty($_POST['content'])) {
            $this->OutputFailure(array(), "Content is not selected");
        } else {
            $pictureFileSys = IMAGE_UPLOAD . "news_avatars/" . $_FILES['avatar']['name'];
            $pictureUrl = APP_IMAGE_URL . "/news_avatars/" . $_FILES['avatar']['name'];
            $this->InputTableNews();
            move_uploaded_file($_FILES['avatar']['tmp_name'], $pictureFileSys);
            $this->$this->OutputSuccess(array(), "News uploaded successful!");
        }
    }

    public function InputTableNews()
    {
        global $data, $pictureUrl;
        $news = R::dispense('news');
        $news['category'] = $_POST['categories'];
        $news['idauthor'] = $this->InputIdAuthor(); //page
        $news['idtags'] = $this->InputIdTags();
        $news['idsong'] = $this->InputIdSong();
        $news['idstory'] = $_POST['namestory'];
        $news['likes'] = $this->InputLikesNews();
        $news['additionTime'] = $data->DateYMD();
        $news['linksong'] = $this->InputLinkSongs(); //page
        $news['linkpicture'] = $pictureUrl; //page
        $news['title'] = $_POST['title']; //page
        $news['content'] = $_POST['content'];
        R::store($news);
    }

    public function InputIdAuthor()
    {
        $idAuthor = $_POST['nameauthor'];
        $idAuthorDb = R::findOne("author", "author_name = ?", [$idAuthor]);
        return $idAuthorDb['id'];
    }

    public function InputIdTags()
    {
        $this->InputNameTags();
        $nameTags = explode(",", $_POST['nametags']);
        $arrayTags = [];
        foreach ($nameTags as $key => $value) {
            $idTag = R::getRow("SELECT * FROM tags WHERE name_tag = ?", [$value]);
            $arrayTags[$key] = $idTag['id'];
        }
        $stringTags = implode(",", $arrayTags);
        return $stringTags;
    }

    public function InputNameTags()
    {
        $nameTags = explode(",", $_POST['nametags']);
        $arrayTags = [];
        foreach ($nameTags as $key => $value) {
            $tagid = R::getRow("SELECT * FROM tags WHERE name_tag = ?", [$value]);
            $arrayTags[$key] = $value;
            if (!$tagid) {
                $tagsDb = R::dispense('tags');
                $tagsDb['name_tag'] = $value;
                R::store($tagsDb);
            }
        }
        $stringTags = implode(",", $arrayTags);
        return $stringTags;

    }

    public function InputIdSong()
    {

        $arraySong = explode(",", $_POST['song']);
        $arrayManySong = [];
        foreach ($arraySong as $key => $value) {
            $idSong = R::getRow("SELECT * FROM music WHERE filename = ?", [$value]);
            $arrayManySong[$key] = $idSong['id'];
        }
        $arraySong = implode(",", $arrayManySong);
        return $arraySong;

    }

    public function InputLikesNews()
    {
        global $likes;
        $id = R::getRow("SELECT * FROM news WHERE nameauthor = ?", [$_POST['nameauthor']]);
        @$likes = R::findAll("likesnews", "id_news = ?", [$id['id']]);
        if ($likes == null) {
            $likes = 0;
            return count($likes);
        } else {
            return count($likes);
        }

    }

    public function InputLinkSongs()
    {
        $linkSongUrl = $this->CheckPost("song");

        if ($linkSongUrl != "null") {
            $array = [];
            $arraySongs = explode(",", $linkSongUrl);
            foreach ($arraySongs as $value) {
                $array[] = APP_MEDIA . "/" . $value;
            }
            $stringSongs = implode(",", $array);
            return $stringSongs;
        }
    }

    public function AddingStory()
    {
        global $pictureUrl;

        $this->CheckPost('category');
        $this->CheckPost('nametags');
        $this->CheckPost('content');
        $this->CheckPost('title');

        if (empty($_FILES['avatar']['name'])) {
            $this->OutputFailure(array(), "File dont choose!");
        } elseif (empty($_POST['nametags'])) {
            $this->OutputFailure(array(), "Tag is not selected");
        } elseif (empty($_POST['title'])) {
            $this->OutputFailure(array(), "Title is not selected");
        } elseif (empty($_POST['content'])) {
            $this->OutputFailure(array(), "Content is not selected");
        } else {
            $pictureUrl = APP_IMAGE_URL . "/stories_avatars/" . $_FILES['avatar']['name'];
            $pictureFileSys = IMAGE_UPLOAD . "stories_avatars/" . $_FILES['avatar']['name'];
            $this->InputTableStory();
            move_uploaded_file($_FILES['avatar']['tmp_name'], $pictureFileSys);
            $this->OutputSuccess(array(), "Story uploaded successful!");
        }
    }

    public function InputTableStory()
    {
        global $pictureUrl;
        $news = R::dispense('news');
        $news['category'] = $this->CheckPost('category');
        $news['idtags'] = $this->InputIdTagsStory();
        $news['linkpicture'] = $pictureUrl; //page
        $news['title'] = $_POST['title']; //page
        $news['content'] = $_POST['content'];
        R::store($news);
    }

    public function InputIdTagsStory()
    {
        $nameTags = explode(",", $_POST['nametags']);
        $arrayTags = [];
        foreach ($nameTags as $key => $value) {
            $idTag = R::getRow("SELECT * FROM tags WHERE name_tag = ?", [$value]);
            $arrayTags[$key] = $idTag['id'];
            if (!$idTag) {
                $tagsDb = R::dispense('tags');
                $tagsDb['name_tag'] = $value;
                R::store($tagsDb);
                $idNewTag = R::getRow("SELECT * FROM tags WHERE name_tag = ?", [$value]);
                $arrayTags[$key] = $idNewTag['id'];
            }
        }
        $stringTags = implode(",", $arrayTags);
        return $stringTags;
    }

    public function AddingTagsToUser()
    {
        if (!empty($_SESSION['username'])) {

            $user = R::findOne('users', 'user_email = ?', [$_SESSION['username']]);
            if (!$user) {
                $user = R::findOne('users', 'user_idvk = ?', [$_SESSION['username']]);
            } else if (!$user) {
                $user = R::findOne('users', 'user_facebookid = ?', [$_SESSION['username']]);
            } else if (!$user) {
                $user = R::findOne('users', 'user_googleid = ?', [$_SESSION['username']]);
            }

            $arrayUserTags = explode(",", $user['tags_ids']);
            $arrayIdTag = explode(",", $this->CheckPost('idtag'));

            if (in_array($this->CheckPost('idtag'), $arrayUserTags)) {
                $diffArray = array_diff($arrayUserTags, $arrayIdTag);
                $diffArray = array_diff($diffArray, array(''));
                $diffString = implode(",", $diffArray);
                $user['tags_ids'] = $diffString;

                R::store($user);
                $this->OutputSuccess(array(), 'Delete');
            } else {
                $diffArray = array_diff($arrayIdTag, $arrayUserTags);
                $diffArray = array_merge($arrayUserTags, $diffArray);
                $diffArray = array_diff($diffArray, array(''));

                $diffString = implode(",", $diffArray);
                $user['tags_ids'] = $diffString;

                R::store($user);
                $this->OutputSuccess(array(), 'Add');
            }
        } else {
            $this->OutputFailure(array(), 'Please auth.');
        }
    }

//VERIFY LOGIN AND PASSWORD

    public function DeleteNews()
    {
        $idNews = $this->CheckPost('idnews');
        $newsDb = R::findOne("news", "id = ?", [$idNews]);
        R::trash($newsDb);
        $this->OutputSuccess(array(), "This news deleted");
    }

    public function AddToPlaylist()
    {
        if (!empty($_SESSION['username'])) {

            $user = R::findOne('users', 'user_email = ?', [$_SESSION['username']]);
            if (!$user) {
                $user = R::findOne('users', 'user_idvk = ?', [$_SESSION['username']]);
            } else if (!$user) {
                $user = R::findOne('users', 'user_facebookid = ?', [$_SESSION['username']]);
            } else if (!$user) {
                $user = R::findOne('users', 'user_googleid = ?', [$_SESSION['username']]);
            }

            $play = R::dispense('usersplaylist');
            $$this->Checkplay = R::findOne('usersplaylist', 'idnews = ? AND userid = ?',
                [$this->CheckPost('idnews'), $user['id']]);

            if (!$$this->Checkplay) {
                $play['idnews'] = $this->CheckPost('idnews');
                $play['userid'] = $user['id'];
                R::store($play);

                AddIdsPlaylist();
                $this->OutputSuccess(array(), 'Delete from playlist');
            } else {
                MinusIdsPlaylist();
                R::trash($$this->Checkplay);
                $this->OutputSuccess(array(), 'Add to playlist');
            }
            $this->OutputFailure(array(), 'Please auth.');
        }
    }

//UPDATE USER CONFIG

    public function SearchMusic()
    {
        R::dispense('testmusic');
        $word = '%' . $this->CheckPost('name') . '%';
        $search = R::findAll('music', 'filename LIKE ? ', [$word]);
        if ($_POST['name'] == '*') {
            $word = '%';
            $search = R::findAll('music', 'filename LIKE ? ', [$word]);
            $this->ShowSongs($search);
        } elseif ($_POST['name'] == '') {
            $this->OutputFailure(array(), "Input song name!");
        } elseif (!$search && !empty($_POST['name'])) {
            $this->OutputFailure(array(), "Song don't find!");
        } elseif ($search && !empty($_POST['name'])) {
            $this->ShowSongs($search);
        }
    }

//LOG OUT USER

    public function ShowSongs($search)
    {
        $jsonMas = [];
        foreach ($search as $key => $value) {
            $pathFile = APP_MEDIA . "/" . $value['filename'];

            $arr = array(
                'id' => $value['id'],
                'name' => $value['orgname'],
                'source' => $pathFile,
                'description' => $value['description'],
                'author' => $value['author'],
                'likes' => $value['likes'],
                'fullname' => $value['filename']);
            //$arr = str_replace("_"," ",$arr);
            $jsonMas[$key] = $arr;
        }
        $this->OutputSuccess($jsonMas, JSON_FORCE_OBJECT);
    }

//DELETE USER FROM SERVER UHAHA

    public function UploadMusic()
    {
        global $originalName, $authorDb, $songName, $md5File, $key;

        $output = NULL;
        $outputInfo = NULL;
        if (!empty($_FILES)) {

            $output = NULL;

            foreach ($_FILES as $key => $value) {
                $originalName = preg_replace("/.mp3|.mp4|.mpeg/i", "", $this->SongFullName($key));

                $authorDb = $this->AuthorInSong($originalName);
                $songName = $this->SongName($originalName);

                $md5File = md5_file($_FILES[$key]['tmp_name']);

                @$md5FileDb = R::getRow('SELECT * FROM music WHERE md5 = ?', [$md5File]);

                if ($md5File == $md5FileDb['md5']) {
                    $output = "$authorDb - $songName: song is already there! Download something else!\n";
                } else {

                    $outputInfo = " Here is some more debugging info: $authorDb - $songName";

                    if (move_uploaded_file($_FILES[$key]['tmp_name'], MUSIC_UPLOAD . $this->SongFullName($key))) {
                        $this->CreateDatabaseSong();
                        $output = "UPLOAD_SECCESS \n" . $outputInfo;
                    } else {
                        $output = "UPLOAD_FAIL \n" . $outputInfo;
                    }
                }
            }
            $this->OutputSuccess(array(), $output);
        } else {
            $this->OutputFailure("Input FILES!!!");
        }
    }

//AddToPlaylist NEWS

    public function SongFullName($key)
    {
        $fileNameWithoutMinus = preg_replace("/\–/i", "-", $_FILES[$key]['name']);
        $lowFileNameWithoutMinus = strtolower($fileNameWithoutMinus);
        $fileNameWithoutLessWord = preg_replace("/(\()|(\))|(_\(zaycev.net\))/i", "", $lowFileNameWithoutMinus);
        $fileName = preg_replace("/(\s)/i", "_", $fileNameWithoutLessWord);
        return $fileName;
    }

    public function AuthorInSong($orgname)
    {
        $author = preg_replace("/\-\w+$/i", " ", $orgname);
        $author = preg_replace("/\_/", " ", $author);
        $author = preg_replace("/\s+$/", "", $author);
        $author = mb_convert_case($author, MB_CASE_TITLE, "UTF-8");
        return $author;
    }

    public function SongName($orgname)
    {
        $song = preg_replace("/^\w+\-/i", " ", $orgname);
        $song = preg_replace("/\_/", " ", $song);
        $song = preg_replace("/^\s+/", "", $song);
        $song = mb_convert_case($song, MB_CASE_TITLE, "UTF-8");
        return $song;
    }

//Add tag to user

    public function CreateDatabaseSong()
    {
        global $data, $authorDb, $originalName, $songName, $md5File, $key;
        $musics = R::dispense('music');
        $musics["filename"] = $this->SongFullName($key);
        $musics["description"] = $_POST["comment"];
        $musics["author"] = $authorDb;
        $musics["songname"] = $songName;
        $musics["date"] = $this->DateYMD();
        $musics["orgname"] = $originalName;
        $musics["md5"] = $md5File;
        $musics["chrono"] = $this->Chronometry(MUSIC_UPLOAD . "/" . $this->SongFullName($key));
        $musics["likes"] = 0;
        R::store($musics);
    }

//GET LIKE ONE SONG FOR PAGE_SONG

    public function Chronometry($file)
    {
        $chrono = new MP3File($file);
        $duration = $chrono->getDuration();
        $duration = $chrono->formatTime($duration);
        return $duration;
    }

//GET SESSION

    public function UpdateUser()
    {
        $user = R::findOne("users", "user_email = ?", [$_SESSION['username']]);
        if (!$user) {
            $user = R::findOne('users', 'user_idvk = ?', [$_SESSION['username']]);
        } else if (!$user) {
            $user = R::findOne('users', 'user_facebookid = ?', [$_SESSION['username']]);
        } else if (!$user) {
            $user = R::findOne('users', 'user_googleid = ?', [$_SESSION['username']]);
        }

        if (!empty($_POST['username'])) {
            if ($this->ValidUsername() == "Success") {
                $user['user_login'] = $this->CheckPost('username');
            }
        }
        if (!empty($_POST['email'])) {
            if ($this->ValidEmail() == "Success") {
                $user['user_email'] = $this->CheckPost('email');
                $_SESSION['username'] = $this->CheckPost('email');
            }
        }
        if (!empty($_FILES['avatar'])) {
            $pictureFileSys = IMAGE_UPLOAD . $_FILES['avatar']['name'];
            move_uploaded_file($_FILES['avatar']['tmp_name'], $pictureFileSys);
            $avatar = APP_IMAGE_URL . "/user_avatars/" . $_FILES['avatar']['name'];
            $user['avatar'] = $avatar;
        }
        R::store($user);

    }

//GET AUTHOR SONG DATA

    public function LikeSong()
    {
        $user = R::findOne('users', 'user_email = ?', [$_SESSION['username']]);
        if (!$user) {
            $user = R::findOne('users', 'user_idvk = ?', [$_SESSION['username']]);
        }
        if (!$user) {
            $user = R::findOne('users', 'user_facebookid = ?', [$_SESSION['username']]);
        }
        if (!$user) {
            $user = R::findOne('users', 'user_googleid = ?', [$_SESSION['username']]);
        }

        $iduser = $user['id'];
        $idsong = $this->CheckPost('song_id');

        $likes = R::findOne("likessongs", "id_user = ? AND id_song = ?", [$iduser, $idsong]);

        if (!$likes) {
            $likes = R::dispense('likessongs');
            $likes["idUser"] = $iduser;
            $likes["idSong"] = $idsong;
            R::store($likes);
            $this->OutputSuccess(array(), "1");
        } else {
            R::trash($likes);
            $this->OutputSuccess(array(), "0");
        }
    }

//VIEW NEWS WITH idtags

    public function AddLikeNewsByIdNews()
    {
        if (!empty($_SESSION['username'])) {
            $user = R::findOne('users', 'user_email = ?', [$_SESSION['username']]);
            if (!$user) {
                $user = R::findOne('users', 'user_idvk = ?', [$_SESSION['username']]);
            } else if (!$user) {
                $user = R::findOne('users', 'user_facebookid = ?', [$_SESSION['username']]);
            } else if (!$user) {
                $user = R::findOne('users', 'user_googleid = ?', [$_SESSION['username']]);
            }

            $likeNews = R::findOne('likesNews', 'news_id = ? AND user_id = ?', [$this->CheckPost('idnews'), $user['id']]);

            $news = R::findOne('news', 'id = ?', [$this->CheckPost('idnews')]);

            if (!$likeNews) {
                $dispNews = R::dispense('likesnews');
                $dispNews['news_id'] = $this->CheckPost('idnews');
                $dispNews['user_id'] = $user['id'];
                R::store($dispNews);

                $news['likes'] = $news['likes'] + 1;
                R::store($news);
                $this->AddIdsLikes();
                $this->OutputSuccess(array(), $news['likes']);
            } else {
                $news['likes'] = $news['likes'] - 1;
                R::store($news);
                $this->MinusIdsLikes();
                R::trash($likeNews);
                $this->OutputSuccess(array(), $news['likes']);
            }
        } else {
            $this->OutputFailure(array(), 'Please auth.');
        }
    }

//ADD LIKE FOR NEWS BY USER

    public function AddIdsLikes()
    {
        $user = R::findOne('users', 'user_email = ?', [$_SESSION['username']]);
        if (empty($_SESSION['username'])) {
            $this->OutputFailure(array(), 'Please auth.');
        } else if (!$user) {
            $user = R::findOne('users', 'user_idvk = ?', [$_SESSION['username']]);
        } else if (!$user) {
            $user = R::findOne('users', 'user_facebookid = ?', [$_SESSION['username']]);
        } else if (!$user) {
            $user = R::findOne('users', 'user_googleid = ?', [$_SESSION['username']]);
        }
        $userLikes = explode(",", $user['news_liked_ids']);
        $newsLikes = explode(",", $this->CheckPost('idnews'));
        $array = array_diff($newsLikes, $userLikes);
        $userLikes = array_merge($userLikes, $array);
        $userLikes = array_diff($userLikes, array(''));
        $userTagsString = implode(",", $userLikes);
        $user['news_liked_ids'] = $userTagsString;
        R::store($user);
    }

    public function MinusIdsLikes()
    {
        $user = R::findOne('users', 'user_email = ?', [$_SESSION['username']]);
        if (empty($_SESSION['username'])) {
            $this->OutputFailure(array(), 'Please auth.');
        } else if (!$user) {
            $user = R::findOne('users', 'user_idvk = ?', [$_SESSION['username']]);
        } else if (!$user) {
            $user = R::findOne('users', 'user_facebookid = ?', [$_SESSION['username']]);
        } else if (!$user) {
            $user = R::findOne('users', 'user_googleid = ?', [$_SESSION['username']]);
        }

        $userLikes = explode(",", $user['news_liked_ids']);
        $newsLikes = explode(",", $this->CheckPost('idnews'));
        $array = array_diff($userLikes, $newsLikes);
        $userLikes = array_diff($array, array(''));
        $userTagsString = implode(",", $userLikes);
        $user['news_liked_ids'] = $userTagsString;
        R::store($user);
    }

    public function GetLikesForOneSong()
    {
        $quantitySongs = R::getRow('SELECT * FROM music WHERE filename = ?', [$_POST['fullname']]);
        $this->OutputSuccess($quantitySongs);
    }

//VALID/INVALID USERNAME

    public function DeleteTable()
    {
        $argTable = $this->CheckPost("table");
        $argId = $this->CheckPost("id");
        R::exec("DELETE FROM $argTable WHERE id = $argId");
        $this->OutputSuccess(array(), "Drop success");
    }

//VALID/INVALID PASSWORD

    public function MatchPasswords()
    {
        $pass = $_POST["password"];
        $repass = $_POST['repassword'];
        if ($pass == $repass) {
            return "Success";
        } else {
            return "Passwords not match!";
        }
    }


//VALID/INVALID EMAIL

    public function AddIdsPlaylist()
    {

        $user = R::findOne('users', 'user_email = ?', [$_SESSION['username']]);

        if (!$user) {
            $user = R::findOne('users', 'user_idvk = ?', [$_SESSION['username']]);
        } else if (!$user) {
            $user = R::findOne('users', 'user_facebookid = ?', [$_SESSION['username']]);
        } else if (!$user) {
            $user = R::findOne('users', 'user_googleid = ?', [$_SESSION['username']]);
        }
        $userTagsMassiv = explode(",", $user['news_added_ids']);
        $newsTagsMassiv = explode(",", $this->CheckPost('idnews'));
        $array = array_diff($newsTagsMassiv, $userTagsMassiv);

        $userTagsMassiv = array_merge($userTagsMassiv, $array);
        $userTagsMassiv = array_diff($userTagsMassiv, array(''));

        $userTagsString = implode(",", $userTagsMassiv);
        $user['news_added_ids'] = $userTagsString;
        R::store($user);

    }

//MATCH PASSWORDS

    public function MinusIdsPlaylist()
    {
        $user = R::findOne('users', 'user_email = ?', [$_SESSION['username']]);
        if (!$user) {
            $user = R::findOne('users', 'user_idvk = ?', [$_SESSION['username']]);
        } else if (!$user) {
            $user = R::findOne('users', 'user_facebookid = ?', [$_SESSION['username']]);
        } else if (!$user) {
            $user = R::findOne('users', 'user_googleid = ?', [$_SESSION['username']]);
        }

        $userTagsMassiv = explode(",", $user['news_added_ids']);
        $newsTagsMassiv = explode(",", $this->CheckPost('idnews'));

        $array = array_diff($userTagsMassiv, $newsTagsMassiv);

        $userTagsMassiv = array_diff($array, array(''));

        $userTagsString = implode(",", $userTagsMassiv);
        $user['news_added_ids'] = $userTagsString;
        R::store($user);
    }
}
new Dispatcher();

?>