<?php
require_once('./page_public.php');

class SearchPage extends page_public
{
    protected function Content()
    {
        $this->AppendScript("sendMusic");
        $page_search = "
            <div class='main-text-name'>Поиск песен</div>
            <hr></hr>
            
            <form class='input-group inpusearch-song-inp'>
                <input class=\"form-control \" id=\"name\" type=text value=\"\" size=\"20\" autocomplete='off'/>
                <div class='input-group-btn'>
                    <input class=\"btn btn-default \" type=\"submit\" onclick=\"sendMusic(); return false;\" value='Поиск'>
                </div>
            </form>
            <div id=\"player\"></div>
            <div id=\"song\"></div>
            <div id=\"dataSong\"></div>";
        return $page_search;
    }
}

$search = new SearchPage();
$search->DisplayPage();
?>

