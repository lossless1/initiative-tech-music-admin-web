<?php
require_once ('../page_user.php');

class Subscriptions extends page_user{
    protected  function Content()
    {
        $this->AppendScripts('page_subscriptions');
        $user_subs = <<<SUB
        <center>
        <div id="subscriptions"></div>
        </center>
SUB;
        return $user_subs;
    }
}
$subs = new Subscriptions();
$subs->DisplayPage();
?>