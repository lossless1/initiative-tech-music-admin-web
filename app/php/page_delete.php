<?php
require_once ("page_public.php");
class Delete extends page_public{
    public function Content()
    {
        $this->AppendScript("page_delete");
        $this->AppendAssetScript('/js/select2/dist/js/select2.full');
        $this->AppendAssetCss('js/select2/dist/css/select2');
        $deletePage = <<<DEL
        <table border="0" align="center">
                <tbody id="info">
                </tbody>
        </table>
        <div id="infoByButton"></div>
DEL;
        return $deletePage;
    }
}
$delete = new Delete();
$delete->DisplayPage();