<?php

require_once('page_public.php');

class Song extends page_public
{
    protected function Content()
    {
        $this->AppendScript('sendMusic');
        $this->AppendScript('page_song');
        $page_song = <<<PGS

        <div id="music">
            Stranica s loisom!<br><br>
        </div>

PGS;
        return $page_song;
    }
}

$song = new Song();
$song->DisplayPage();
//$song->Likes();