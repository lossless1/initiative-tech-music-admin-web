<?php
require_once('./page_user.php');

class Options extends page_user
{
    public function Content()
    {
        $this->AppendScript('user_options');
        $options = <<<OPT
        </br >
        Rename user:
        <input id="username_update" type="text"/></br >
        <div id="error_username" style="color:red;"></div></br >
        Rename email:
        <input id="email_update" type="text"/></br >
        <div id="error_email" style="color:red;"></div></br >
        Avatar file:
        <input id="avatar_update" type="file" accept="image/*"/></br ></br >
        <button id="submit">Сохранить</button>
        <button id="deleteuser">Удалиться</button>
        <div id="info"></div>
OPT;
        return $options;
    }
}
$optionsClass = new Options();
$optionsClass->DisplayPage();