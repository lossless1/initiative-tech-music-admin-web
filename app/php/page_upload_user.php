<?php
require_once('page_public.php');

class Upload extends page_public
{
    protected function Content()
    {
        $this->AppendScript('sendFile');
        $upload = "        
            Отправить этот файл: <input type=\"file\" id=\"fileinput\" accept=\"audio/*\" multiple=\"multiple\"/>
            <input id=\"buttonUpload\" type=\"button\" class=\"btn btn-default\" value=\"Загрузка\" \"/><br><br>
            Комментарии к песне: <input type=text id=\"description\" size=50/><br><br>

        <br>
        <div id=\"data\"><br>
        </div>
        <div class='progress' id=\"progress\">
            <div class='progress-bar' role='progressbar' aria-valuenow='' aria-valuemin=\"0\" aria-valuemax=\"100\">
                
            </div>
        </div>
        <progress class=\"progress\" ></progress>
        ";
        return $upload;
    }
}

$search = new Upload();
$search->DisplayPage();
?>

