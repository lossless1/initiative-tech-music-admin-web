<?php

require_once('../dataprocessing.php');

class VkLogin extends dataprocessing
{
    public function VkLogin()
    {
        $client_id = '5388901'; // Client ID
        $redirect_uri = APP_PHP_URL . '/auth/vk_auth.php'; // Redirect URIs
        $display = 'mobile';
        $client_secret = 'Ea2ykpIefSH0kqvieOg8'; // Client secret
        $response_type = 'code';
        $scope = 'offline';
        $v = '5.50';


        $tokenInfo = null;
        $result = null;
        $url = 'https://oauth.vk.com/authorize';

        $params = array(
            'client_id' => $client_id,
            'display' => $display,
            'redirect_uri' => $redirect_uri,
            'response_type' => $response_type,
            'scope' => $scope,
            'v' => $v
        );

        echo $link = '<p><a href = "' . $url . '?' . urldecode(http_build_query($params)) . '"><img src="' . APP_IMAGE_URL . '/vk_login.png"></a></p>';


        if (isset($_GET['code'])) {
            $params = array(
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'redirect_uri' => $redirect_uri,
                'code' => $_GET['code']
            );

            $url = 'https://oauth.vk.com/access_token';

            $tokenInfo = json_decode(file_get_contents($url . '?' . urldecode(http_build_query($params))), true);

        }
        $userInfo = null;
        if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {

            $params = array(
                'access_token' => $tokenInfo['access_token']

            );
            $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);
            //users.get  https://vk.com/dev/fields

            var_dump($userInfo);

            if (isset($userInfo['response'][0]['uid'])) {

                $this->ShowInfo($userInfo);
            }
        }
    }

    public function ShowInfo($userInfo)
    {
        $userId = $userInfo['response'][0]['uid'];
        $_SESSION['username'] = $userInfo['response'][0]['uid'];
        setcookie('username', $_SESSION['username'], time() + 3600, '/');
        $this->ConnectDB();

        $checkuser = R::findOne('users', 'user_idvk = ?', [$userId]);
        if (!$checkuser) {
            $users = R::dispense('users');
            $users['user_idvk'] = $userInfo['response'][0]['uid'];
            $users['user_login'] = $userInfo['response'][0]['first_name'] . " " . $userInfo['response'][0]['last_name'];
            R::store($users);

            header("Location:" . APP_PHP_URL . "/page_search_public.php");
        } else {
            header("Location: " . APP_PHP_URL . "/page_search_public.php");
        }

    }
}


$facebook = new VkLogin();
?>

