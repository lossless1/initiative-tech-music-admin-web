<?php

require_once('../dataprocessing.php');

class GoogleAutorization extends dataprocessing
{
    public function GoogleAutorization()
    {

        $client_id = '876652444885-gj17d7alqvgsv1kqn1gl4b0o7ctptfd5.apps.googleusercontent.com'; // Client ID
        $client_secret = 'hdZcbOhNRKM141mi1H-KJZEc'; // Client secret
        $redirect_uri = APP_PHP_URL . '/auth/google_auth.php'; // Redirect URI

        $result = null;

        $url = 'https://accounts.google.com/o/oauth2/v2/auth';

        $params = array(
            'redirect_uri' => $redirect_uri,
            'response_type' => 'code',
            'client_id' => $client_id,
            'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
        );


        echo $link = '<a  href="' . $url . '?' . urldecode(http_build_query($params)) . '"><img src="' . APP_IMAGE_URL . '/google_plus_login.png"></a>';

        //echo $link = '<p><a  href="' . $url . '?' . urldecode(http_build_query($params)) . '"><img src="' . APP_IMAGE_URL . '/google_plus_login.png"></a></p>';


        // https://accounts.google.com/o/oauth2/auth?redirect_uri=http://localhost/google-auth&response_type=code&client_id=333937315318-fhpi4i6cp36vp43b7tvipaha7qb48j3r.apps.googleusercontent.com&scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile
        if (!empty($_GET['code'])) {

            $params = array(
                'code' => $_GET['code'],
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'redirect_uri' => $redirect_uri,
                'grant_type' => 'authorization_code'
            );
            $url = 'https://www.googleapis.com/oauth2/v4/token';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curl);
            curl_close($curl);


            $tokenInfo = json_decode($result, true);

            if (!empty($tokenInfo['access_token'])) {
                $params['access_token'] = $tokenInfo['access_token'];
                $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);
                //echo "<pre>";
                //var_dump($userInfo);
                if (isset($userInfo['id'])) {
                    $this->ShowInfo($userInfo);
                }
            }
        }
    }

    public function ShowInfo($userInfo)
    {
        $_SESSION['username'] = $userInfo['email'];
        setcookie('username', $_SESSION['username'], time() + 3600, '/');
        $this->ConnectDB();

        $checkuser = R::findOne('users', 'user_googleid = ?', $userInfo['id']);
        if (!$checkuser) {
            $users = R::dispense('users');
            $users['user_login'] = $userInfo['name'];
            $users['user_email'] = $userInfo['email'];
            $users['user_googleid'] = $userInfo['id'];
            //$users['user_link_to_profile'] = $userInfo['link'];   //link on profile
            //$users['user_picture'] = '<img src="' . $userInfo['picture'] . '" />'; //picture on profile
            R::store($users);
            header("Location: " . APP_PHP_URL . "/page_search_public.php");
        } else {
            header("Location: " . APP_PHP_URL . "/page_search_public.php");
        }
    }
}

$google = new GoogleAutorization();
?>