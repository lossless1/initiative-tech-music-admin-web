var lang, buttonRegister, data, divRegister, username, password, rePassword, email, repassword;

var home = APP_PHP_URL + "/page_search_public.php";
var URL = APP_PHP_URL + "/dispatcher.php?command=";

$(document).ready(function () {
    lang = getParameterByName("language");

    buttonRegister = $("#register");
    divRegister = $("#divRegister");
    buttonRegister.on("click", buttonRegister_Click);
    buttonRegister = $("#button_log");
    buttonRegister.on("click", buttonLogin_Click);

    $("#username_register").blur(function () {

        username = $("#username_register").val();

        $.post(URL + "valid_username", {
                username: username
            },
            function (data) {
                json = JSON.parse(data);
                $("#error_login").html(json.message);
            });
    });// not ok

    $("#password_register").blur(function () {
        password = $("#password_register").val();
        $.post(URL + "valid_password", {
                password: password
            },
            function (data) {
                json = JSON.parse(data);
                $("#error_password").html(json.message);
                MatchPassword();
            });
    });//ok

    $("#reenter_password_register").blur(function () {

        rePassword = $("#reenter_password_register").val();
        $.post(URL + "valid_repassword", {
                repassword: rePassword
            },
            function (data) {
                json = JSON.parse(data);
                $("#error_repassword").html(json.message);
                MatchPassword();
            });
    });//ok

    $("#email_register").blur(function () {
        email = $("#email_register").val();

        $.ajax({
            method: 'POST',
            url: URL + "valid_email",
            data: {
                email: email
            },
            success: function (result) {
                json = $.parseJSON(result);

                $("#error_email").html(json.message);
            }
        });

    });//ok
});
function MatchPassword() {
    password = $("#password_register").val().length;
    rePassword = $("#reenter_password_register").val().length;
    $.post(URL + "match_passwords", {
        password: password,
        repassword: rePassword
    }, function (data) {

        json = JSON.parse(data);
        $("#error_match").html(json.message);

    });
}

function buttonLogin_Click() {
    $.ajax({
        method: 'POST',
        url: URL + "authentication",
        ContentType: 'application/html',
        data: {
            user_email: $("#user_email").val(),
            user_password: $("#user_password").val(),
            language: lang
        },
        success: function (result) {
            data = JSON.parse(result);
            $("#infoLogin").empty().html(data.message);
            console.log(result);
            if (data.result == "SUCCESS") {
                window.location.assign(home);
            }

        }
    });
    return false;
}

function buttonRegister_Click() {

    username = $("#username_register");
    password = $("#password_register");
    repassword = $("#reenter_password_register");
    email = $("#email_register");
    $.ajax(
        {
            method: 'POST',
            url: URL + "user_registration",
            ContentType: 'application/json',
            data: {
                username: username.val(),
                password: password.val(),
                repassword: repassword.val(),
                email: email.val(),
                language: lang
            },
            success: function (data) {

                $("#infoReg").empty();
                json = $.parseJSON(data);

                $("#infoReg").html(json.message);
            }
        });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}