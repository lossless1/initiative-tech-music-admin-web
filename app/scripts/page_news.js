var aTag, divTag, json;
var url = APP_PHP_URL + "/dispatcher.php?command=";
$(document).ready(function () {
    $.get(url + "get_news",
        function success(data) {
            json = JSON.parse(data);

            $.each(json.data, function (key, value) {
                ShowIdNews(value);
            });
        });
});

function ShowIdNews(value) {
    divTag = $("#infoNews");
    aTag = $("<a>");
    aTag.attr("href", APP_PHP_URL + "/page_show_news.php?id=" + value.id);
    aTag.html(value.id + ' ' + value.category);
    divTag.append("<br>");
    divTag.append(aTag);
}
