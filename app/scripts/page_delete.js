var url = APP_PHP_URL + "/dispatcher.php?command=";
$(document).ready(function(){
    OptionAllTables();
    OptionDataTables();
    DeleteButton();

});
function OptionAllTables(){

    tbody = $("#info");
    tr = $("<tr>");
    tr.attr("id", "table");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Таблицы (Tables)");

    tdselect = $("<td>");
    select = $("<select>");
    select.attr("id", "tables");
    select.append("<option>");

    tr.append(td);
    tdselect.append(select);
    tr.append(tdselect);
    tbody.append(tr);

    $.get(url + 'get_tables', //list authors Select 2
        function success(info) {
            json = JSON.parse(info);
            if(json.data.length == 0){
                $("#tables").select2({
                    width: 250,
                    placeholder: "Select a table to drop"
                });
            }
            $.each(json.data, function (key, value) {
                $("#tables").select2({
                    width: 250,
                    placeholder: "Select a table to drop",
                    allowClear: true,
                    data: [{
                        id: value, text: value
                    }]
                });
            })
        });
}
function OptionDataTables(){
    tbody = $("#info");
    tr = $("<tr>");
    tr.attr("id", "data");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Информация (Data)");

    tdselect = $("<td>");
    select = $("<select>");
    select.attr("id", "information");
    select.append("<option>");

    tr.append(td);
    tdselect.append(select);
    tr.append(tdselect);
    tbody.append(tr);

    $("#information").select2({
        width: 250,
        placeholder: "Select an info to drop"
    });
    $("#tables").change(function(){
        $("#information").empty();
        table = $("#tables").val();
        $.post(url + 'get_information_from_table',
            {
                table:table
            },
            function success(info) {
                json = JSON.parse(info);
                if(json.data.length == 0){
                    $("#information").select2({
                        width: 250,
                        placeholder: "Select an info to drop"
                    });
                }

                var textInformation;
                $.each(json.data, function (key, value) {
                    if(value.filename){
                        textInformation = value.id + "|" + value.filename;
                    }else if(value.category){
                        textInformation = value.id + "|" + value.category
                    }else if(value.author_name){
                        textInformation = value.id + "|" + value.author_name
                    }else if(value.name_tag){
                        textInformation = value.id + "|" + value.name_tag
                    }else if(value.user_email){
                        textInformation = value.id + "|" + value.user_email
                    }
                    $("#information").select2({
                        width: 250,
                        placeholder: "Select an info to drop",
                        allowClear: true,
                        data: [{
                            id: value.id, text: textInformation//value.id + " " + value.filename
                        }]
                    });
                });

            });
    });

}
function DeleteButton(){
    tbody = $("#info");
    tr = $("<tr>");
    tr.attr("id", "story");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Удалить (Delete)");

    tdselect = $("<td>");
    select = $("<button>");
    select.attr("id", "deletebutton");
    select.text("Delete");

    tr.append(td);
    tdselect.append(select);
    tr.append(tdselect);
    tbody.after(tr);

    $("#deletebutton").click(function (){
        data = $("#tables").val();
        info = $("#information").val();
        $.post(url + "delete_table",{
                table:data,
                id:info
            },
            function(data){
                json = JSON.parse(data);
                $("#infoByButton").html(json.message);
            }
        );
    });

}