var author, nameObject, fullname, urlname, audioObject, tagSong,songData;

var phpurl = APP_PHP_URL + "/dispatcher.php?command=";
var urlform = APP_UPLOAD_FORM;


$(document).ready(function () {

    author = getParameterByName('author');

    tagSong = $("#music");


    $.post(phpurl+'get_author_song_data',{
        author:author
    },function success(data){
        songData = JSON.parse(data);

        $.each(songData.data,function(key,value){
            Song(value);
        });
    });
});

function Song(value){
    fullname = value.filename;
    urlname = urlform + "/" + fullname;

    nameObject = NameObject(fullname);
    audioObject = BuildAudioObject(urlname);

    tagSong.append(nameObject);
    tagSong.append(audioObject);
    tagSong.append($("<br>"));
}
function NameObject(value) {
    nameObject = $("<div>");
    nameObject.html(value);
    return nameObject;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
