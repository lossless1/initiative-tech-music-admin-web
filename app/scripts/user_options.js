var data,formData,avatar,email,username;
var url = APP_PHP_URL + "/dispatcher.php?command=";
$(document).ready(function () {

    $("#username_update").blur(function () {

        username = $("#username_update").val();

        $.post(url + "valid_username", {
                username: username
            },
            function (data) {
                json = JSON.parse(data);

                $("#error_username").html(json.message);
                $("#error_username").text(json.message);
            });
    });// not ok

    $("#email_update").blur(function () {
        email = $("#email_update").val();
        $.ajax({
            method: 'POST',
            url: url + "valid_email",

            data: {
                email: email
            },
            success: function (result) {
                data = $.parseJSON(result);

                $("#error_email").html(data.message);
            }
        });

    });//ok

    $("#submit").click(function(){
        username = $("#username_update").val();
        email = $("#email_update").val();
        avatar = $("#avatar_update");

        formData = new FormData;

        $.each(avatar[0].files, function (key, value) {
            formData.append("avatar", value);
        });
        formData.append("username",username);
        formData.append("email",email);


        $.ajax({
            method: "POST",
            url: url + "update_user",
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                console.debug(data);
                $("#info").html(data);

                json = JSON.parse(data);
                $("#info").html(json.message);

            }
        });
    });
    $("#deleteuser").click(function(){
        $.ajax({
            url:url+"delete_user",
            method:"POST",
            success: function(data){
                $("#deleteuser").html(data);
            }
        })
    })
});