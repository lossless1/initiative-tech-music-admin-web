var fullSongData, filename, array, cookie;

var media = APP_MEDIA;
var url = APP_PHP_URL + "/dispatcher.php?command=";

$(document).ready(function () {
    var nameObject, fullname, urlname, audioObject, buttonObject, tagSong;
    tagSong = $("#music");
    filename = getParameterByName('fullname');

    array = ["_(zaycev.net)", ".mp3", ".mp4", ".mpeg", "_"];
    fullname = filename.replace(array, " ");
    cookie = getCookie('username');
    urlname = media + "/" + filename;

    $.post(url + "songslikes", {
        fullname: filename
    }, function success(data) {
        fullSongData = JSON.parse(data);

        if(Boolean(cookie)){
            if (!(cookie.length == "0")) {
                tagSong.append(buttonObject);

                likesdbId(fullSongData);
            }
        }
    });

    audioObject = BuildAudioObject(urlname);
    nameObject = NameObject(fullname);
    buttonObject = ButtonObject();

    tagSong.append(nameObject);
    tagSong.append(audioObject);
    tagSong.append($("<br>"));
});

function likesdbId(data) {
    $("#button").click(function () {
        $.post(url + "like_song",
            {
                song_id: data.data.id
            },
            function success(data) {
                $("#button").html(data);
            });
    });
}

function NameObject(fullname) {
    nameObject = $("<div>");
    nameObject.html(fullname);
    return nameObject;
}
function ButtonObject() {
    buttonObject = $("<button>");
    buttonObject.attr("id", "button");
    buttonObject.html("LAIKNI");
    return buttonObject;
}


function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}