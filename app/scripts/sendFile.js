var url, file, comment, author, form, formData, description;
$(document).ready(function () {
    $("#buttonUpload").click(function () {
        url = APP_PHP_URL + "/dispatcher.php?command=upload_music";

        file = $("#fileinput");
        description = $("#description").val();

        formData = new FormData();
        //console.debug(file[0].files);
        $.each(file[0].files, function (key, value) {
            formData.append(key, value);
        });

        formData.append('comment', description);
        $.ajax(
            {
                method: "POST",
                url: url,

                xhr: function () {
                    var currentXhr;

                    currentXhr = $.ajaxSettings.xhr();

                    if (currentXhr.upload) {
                        currentXhr.upload.addEventListener("progress", progressEventListener, false);
                    }

                    return currentXhr;
                },
                data: formData,
                contentType: false,
                processData: false,

                success: function (data) {
                    json = JSON.parse(data);

                    $("#data").html(json.message);
                }
            });
    });
});
function progressEventListener(event) {
    if (event.lengthComputable) {
        var progressBar;

        progressBar = $("progress");

        progressBar.attr(
            {
                value: event.loaded,
                max: event.total
            });
    }
}