var id, divTag, img, sound, buttonPlaylist, buttonLike, buttonDelete;
var url = APP_PHP_URL + "/dispatcher.php?command=";
var home = APP_PHP_URL + "/page_search_public.php";
$(document).ready(function () {
    id = getParameterByName('id');
    divTag = $("div:first");
    $.post(url + "get_news",
        {
            ids: id
        },
        function (data) {
            json = JSON.parse(data);
            $.each(json.data,function(key,value){
                CreateTitle(value);
                CreateImg(value);
                CreateSong(value);
                CreateButtonAddToPlaylist(value);
                CreateButtonLikeNews(value);
                CreateDeleteButton(value);
            });
        });
});
function CreateTitle(value) {
    title = $("<h3>");
    title.html(value.title);
    divTag.append(title);
}
function CreateImg(value) {
    img = $("<img>");
    img.attr("src", value.linkpicture);
    img.attr("width", "100");
    img.attr("height", "100");
    divTag.append(img);
}

function CreateSong(value) {
    if(value.linksong !== null){
        linksong = value.linksong.split(",");
        $.each(linksong, function (key, value) {
            sound = $("<audio>");
            sound.attr("controls", "on");
            source = $("<source>");
            source.attr("src", value);
            sound.append(source);
            divTag.append(sound);
            divTag.append("<br>");
        })
    }
}

function CreateButtonAddToPlaylist(value) {
    buttonPlaylist = $("<button>");
    buttonPlaylist.attr("id", "buttonPlaylist");
    buttonPlaylist.html("AddToPlaylist");
    divTag.append(buttonPlaylist);

    $("#buttonPlaylist").click(function () {
        $.post(url + "add_to_playlist",
            {
                idnews: value.id
            },
            function (data) {
                json = JSON.parse(data);
                $("#buttonPlaylist").html(json.message);
            })
    })
}
function CreateButtonLikeNews(value) {
    buttonLike = $("<button>");
    buttonLike.attr("id", "buttonLike");
    buttonLike.html("Like");
    divTag.append(buttonLike);

    $("#buttonLike").click(function () {
        $.post(url + "like_news",
            {
                idnews: value.id
            },
            function (data) {
                json = JSON.parse(data);
                $("#buttonLike").html(json.message);
            })
    })
}

function CreateDeleteButton(value) {
    buttonDelete = $("<button>");
    buttonDelete.attr("id", "buttonDelete");
    buttonDelete.html("Delete news");
    divTag.append(buttonDelete);

    $("#buttonDelete").click(function () {
        $.ajax({
            method:"POST",
            url: url + "delete_news_by_id_news",
            data: {idnews: value.id},
            async: true,
            success: function (data) {
                json = JSON.parse(data);
                $("#buttonDelete").html(json.message);
                window.location.assign(home);
            }
        })
    });
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}