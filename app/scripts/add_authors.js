var formData, song, author, avatar, picture, title, tags, categories, content, arg;
var url = APP_PHP_URL + "/dispatcher.php?command=";

$(document).ready(function () {


    $("#addAuthorButton").click(function () {
        var formData, songname, avatar;
        songname = $("#authorname").val();
        avatar = $("#avatar");

        formData = new FormData();

        $.each(avatar[0].files, function (key, value) {
            formData.append("avatar", value);
        });

        formData.append("authorname", songname);


        $.ajax(
            {
                method: "POST",
                url: url + "adding_author",

                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    json = JSON.parse(data);
                    $("#info").html(json.message);
                }
            });
    }); //submit Author

});