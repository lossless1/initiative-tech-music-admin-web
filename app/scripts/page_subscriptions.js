var  authorsInfo;
var url = APP_PHP_URL+"/dispatcher.php?command=";
var urlphp = APP_PHP_URL;
$(document).ready(function(){
    $.get(url+"get_users_subscribers",
        function success(data){
            authorsInfo = JSON.parse(data);
            $.each(authorsInfo.data,function(key,value){
                BuildLink(value);
            });
    });
});
function BuildLink(value){
    tagNameDiv = $("#subscriptions");
    nameTagA = $("<a>");
    nameTagA.attr("href",urlphp+"/page_author.php?author="+value.author);
    nameTagA.html(value.author);
    nameTagA.append("<br>");

    tagNameDiv.append(nameTagA);

}
