var tagDiv,optionsTag;
var url = APP_PHP_URL + "/dispatcher.php?command=";
var urluser = APP_PHP_URL + "/user/exit.php?";
$(document).ready(function () {

    CreateOption();
    CreateLogOut();
});
function CreateOption(){
    tagDiv = $("#cabinet");
    optionsTag = $("<a>");
    optionsTag.attr("href",APP_PHP_URL+"/page_options.php");
    optionsTag.html("Настройки пользователя");
    tagDiv.append(optionsTag);
}
function CreateLogOut(){
    /**
     *<td id="logout"><p align="right"><a href="{$constant('APP_PHP_USER_URL')}/exit.php">Выйти из личного кабинета</a></p>
     */
    tdTag = $("<td>");
    pTag = $("<p>");
    aTag = $("<a>");
    pTag.attr("align","right");
    aTag.attr("href", url+"logout");
    aTag.html("Выйти из личного кабинета");
    pTag.append(aTag);
    tdTag.append(pTag);
    $("tr").append(tdTag);
}