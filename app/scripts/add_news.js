var formData, story, song, author, avatar, picture, title, tags, categories, content, arg;
var url = APP_PHP_URL + "/dispatcher.php?command=";

$(document).ready(function () {

    CreateNewsPage();

    $("#namecategories").change(function () {
        if ($("#namecategories").val() == "News") {
            $("#button").click(function () {

                avatar = $("#picture");

                song = $("#song").val();

                tags = $("#nametags").val();
                author = $("#nameauthor").val();
                story = $("#namestory").val();
                categories = $("#namecategories").val();
                content = $("#content").val();
                title = $("#title").val();

                formData = new FormData();

                $.each(avatar[0].files, function (key, value) {
                    formData.append("avatar", value);
                });

                formData.append("nameauthor", author);
                formData.append("nametags", tags);
                formData.append("namestory", story);
                formData.append("song", song);
                formData.append("title", title);
                formData.append("categories", categories);
                formData.append("content", content);

                $.ajax(
                    {
                        method: "POST",
                        url: url + "adding_news",

                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            json = JSON.parse(data);
                            $("#info").html(json.message);
                        }
                    });
            });//submit News
            $("#nameauthor").prop("disabled", false);
            $("#namestory").prop("disabled", false);
            $("#song").prop("disabled", false);
        }
        if ($("#namecategories").val() == "Articles") {
            $("#button").click(function () {

                avatar = $("#picture");

                song = $("#song").val();

                tags = $("#nametags").val();
                author = $("#nameauthor").val();
                story = $("#namestory").val();
                categories = $("#namecategories").val();
                content = $("#content").val();
                title = $("#title").val();

                formData = new FormData();

                $.each(avatar[0].files, function (key, value) {
                    formData.append("avatar", value);
                });

                formData.append("nameauthor", author);
                formData.append("nametags", tags);
                formData.append("namestory", story);
                formData.append("song", song);
                formData.append("title", title);
                formData.append("categories", categories);
                formData.append("content", content);

                $.ajax(
                    {
                        method: "POST",
                        url: url + "adding_news",

                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            json = JSON.parse(data);
                            $("#info").html(json.message);
                        }
                    });
            });//submit Articles
            $("#nameauthor").prop("disabled", false);
            $("#namestory").prop("disabled", false);
            $("#song").prop("disabled", false);
        }
        if ($("#namecategories").val() == "Story") {
            $("#button").click(function () {

                tags = $("#nametags").val();
                avatar = $("#picture");
                content = $("#content").val();
                title = $("#title").val();
                category = $("#namecategories").val();

                formData = new FormData();

                $.each(avatar[0].files, function (key, value) {
                    formData.append("avatar", value);
                });

                formData.append("category", category);
                formData.append("nametags", tags);
                formData.append("title", title);
                formData.append("content", content);
                $.ajax(
                    {
                        method: "POST",
                        url: url + "adding_story",

                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            json = JSON.parse(data);
                            $("#info").html(json.message);
                        }
                    });
            }); //submit Stories
            $("#nameauthor").prop("disabled", true);
            $("#namestory").prop("disabled", true);
            $("#song").prop("disabled", true);
        }
    });
});

function CreateNewsPage() {
    DeleteAll();

    Categories();
    Authors();
    Story();
    Tags();
    Songs();
    Picture();
    Title();
    Content();
    Submit();
}
//function CreateArticlePage() {
//    DeleteAll();
//    Categories();
//    Authors();
//    TagsNews();
//    //TagsStory();
//    Songs();
//    Picture();
//    Title();
//    Content();
//    Submit();
//}
//function CreateStoryPage() {
//    DeleteAll();
//    Categories();
//    TagsStory();
//    Picture();
//    Title();
//    Content();
//    Submit();
//}

function DeleteAll() {

    $("#category").remove();
    $("#authors").remove();
    $("#tags").remove();
    $("#music").remove();
    $("#image").remove();
    $("#tit").remove();
    $("#cont").remove();
    $("#submit").remove();
}

function Categories() {
    tbody = $("tbody#adding");
    tr = $("<tr>");
    tr.attr("id", "category");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Категория(Category)");

    tdselect = $("<td>");
    select = $("<select>");
    select.attr("id", "namecategories");
    select.append("<option>");

    tr.append(td);
    tdselect.append(select);
    tr.append(tdselect);
    tbody.append(tr);

    $.get(url + 'get_categ', //list tags
        function success(info) {
            json = JSON.parse(info);
            if(json.data.length == 0){
                $("#namecategories").select2({
                    width: 250,
                    placeholder: "Select a Category..."
                });
            }
            $.each(json.data, function (key, value) {
                $("#namecategories").select2({
                    width: 250,
                    maximumInputLength: 20,
                    placeholder: "Select a Category...",
                    data: [{
                        id: value, text: value
                    }]
                });
            });


        });

}

function Authors() {
    tbody = $("tbody#adding tr:last");
    tr = $("<tr>");
    tr.attr("id", "authors");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Авторы (Authors)");

    tdselect = $("<td>");
    select = $("<select>");
    select.attr("id", "nameauthor");
    select.append("<option>");

    tr.append(td);
    tdselect.append(select);
    tr.append(tdselect);
    tbody.after(tr);
    $.get(url + 'get_authors', //list authors Select 2
        function success(info) {
            json = JSON.parse(info);
            if (json.data.length == 0) {
                $("#nameauthor").select2({
                    width: 250,
                    placeholder: "Select an author..."
                });
            }
            $.each(json.data, function (key, value) {

                $("#nameauthor").select2({
                    width: 250,
                    maximumInputLength: 20,
                    placeholder: "Select an author...",
                    allowClear: true,
                    data: [{
                        id: value.author_name, text: value.author_name
                    }]
                });
            })
        });
}

function Tags() {
    tbody = $("tbody#adding tr:last");
    tr = $("<tr>");
    tr.attr("id", "tags");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Тэги (Tags)");

    tdselect = $("<td>");
    select = $("<select>");
    select.attr("id", "nametags");
    select.attr("multiple", "multiple");

    tr.append(td);
    tdselect.append(select);
    tr.append(tdselect);
    tbody.after(tr);

    $.get(url + 'get_tags', //list tags
        function success(info) {
            json = JSON.parse(info);
            if (json.data.length == 0) {
                $("#nametags").select2({
                    tags: true,
                    width: 250,
                    placeholder: "Select or create tags...",
                });
            }
            $.each(json.data, function (key, value) {
                $("#nametags").select2({
                    tags: true,
                    width: 250,
                    placeholder: "Select or create tags...",
                    closeOnSelect: true,
                    data: [{
                        id: value.name_tag, text: value.name_tag
                    }]
                });
            });
        });

}

function Story() {
    tbody = $("tbody#adding tr:last");
    tr = $("<tr>");
    tr.attr("id", "story");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Сюжет (Story)");

    tdselect = $("<td>");
    select = $("<select>");
    select.attr("id", "namestory");
    select.append("<option>");

    tr.append(td);
    tdselect.append(select);
    tr.append(tdselect);
    tbody.after(tr);

    $.get(url + 'get_story', //list authors Select 2
        function success(info) {
            json = JSON.parse(info);
            if(json.data.length == 0){
                $("#namestory").select2({
                    width: 250,
                    placeholder: "Select a story"
                });
            }
            $.each(json.data, function (key, value) {

                $("#namestory").select2({
                    width: 250,
                    placeholder: "Select a story",
                    allowClear: true,
                    data: [{
                        id: value.title, text: value.title
                    }]
                });
            })
        });
}

function Songs() {
    tbody = $("tbody#adding tr:last");
    tr = $("<tr>");
    tr.attr("id", "music");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Песня (Song)");

    tdselect = $("<td>");
    select = $("<select>");
    select.attr("id", "song");
    select.attr("multiple", "multiple");

    tr.append(td);
    tdselect.append(select);
    tr.append(tdselect);
    tbody.after(tr);

    $.get(url + 'get_songs', //list songs
        function success(info) {
            json = JSON.parse(info);
            if (json.data.length == 0) {
                $("#song").select2({
                    tags: true,
                    width: 250,
                    placeholder: "Select songs..."
                });
            }
            $.each(json.data, function (key, value) {
                $("#song").select2({
                    width: 250,
                    tags: true,
                    closeOnSelect: true,
                    maximumInputLength: 20,
                    placeholder: "Select songs...",
                    data: [{
                        id: value.filename, text: value.filename
                    }]
                });
            });
        });

}

function Picture() {

    tbody = $("tbody#adding tr:last");
    tr = $("<tr>");
    tr.attr("id", "image");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Картинка(Picture)");

    tdinput = $("<td>");
    input = $("<input>");
    input.attr("id", "picture");
    input.attr("size", "40");
    input.attr("type", "file");
    input.attr("accept", "image/*");

    tr.append(td);
    tdinput.append(input);
    tr.append(tdinput);
    tbody.after(tr);

}

function Title() {

    tbody = $("tbody#adding tr:last");
    tr = $("<tr>");
    tr.attr("id", "tit");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Заголовок(Title)");

    tdinput = $("<td>");
    input = $("<input>");
    input.attr("id", "title");
    input.attr("class","form-control");
    input.attr("size", "40");
    input.attr("type", "text");

    tr.append(td);
    tdinput.append(input);
    tr.append(tdinput);
    tbody.after(tr);
}

function Content() {
    tbody = $("tbody#adding tr:last");


    tr = $("<tr>");
    tr.attr("id", "cont");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Содержание (Content)");

    tdTextarea = $("<td>");
    textarea = $("<textarea>");
    textarea.attr("id", "content");
    textarea.attr("size", "40");
    textarea.attr("type", "text");
    textarea.attr("rows", "10");
    textarea.attr("cols", "100");
    textarea.attr("class","form-control");

    tr.append(td);
    tdTextarea.append(textarea);
    tr.append(tdTextarea);
    tbody.after(tr);
}

function Submit() {

    tbody = $("tbody#adding tr:last");

    tr = $("<tr>");
    tr.attr("id", "submit");

    td = $("<td>");
    td.attr("align", "right");
    td.html("Отправить(Submit)");

    tdButton = $("<td>");
    button = $("<button>");
    button.attr("id", "button");
    button.attr("size", "40");
    button.attr("type", "text");
    button.attr("rows", "10");
    button.attr("cols", "100");
    button.html("Submit");

    tr.append(td);
    tdButton.append(button);
    tr.append(tdButton);
    tbody.after(tr);
}

