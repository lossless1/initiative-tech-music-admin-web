var form, textID, data, code, json, js, like;
var url = APP_PHP_URL + "/dispatcher.php?command=";
function sendMusic() {
    textID = $('#name').val();

    $.ajax({
        url: url + "search_music",
        method: "POST",
        data: {
            name: textID
        },
        success: function (msg) {
            //$("#player").html(msg); JSON MESSAGE
            json = JSON.parse(msg);
            $("#song").empty();

            if (json.result == 'FAILURE') {
                $("#song").text(json.message);
                return;
            }
            if (typeof json == 'object') {
                $.each(json.data, function (key, value) {
                    CreateTag(value);
                });
            }
        }
    });
}

function CreateTag(data) {
    data.name = data.name.replace(new RegExp('_', 'g'), " ");

    var nameObject, tagSong, audioObject, authorObject;

    tagSong = $("#song");


    nameObject = BuildNameObject(data);
    authorObject = BuildAuthorObject(data);
    audioObject = BuildAudioObject(data.source);

    tagSong.append(nameObject);
    tagSong.append(authorObject);
    tagSong.append(audioObject);
    tagSong.append($("<br>"));
    tagSong.append($("<br>"));
}

function BuildNameObject(data) {
    var name;
    name = $("<div>");
    name.html("<b>Song:</b>");

    nameObject = $("<a>");
    nameObject.attr("href", APP_PHP_URL + "/page_song.php?id=" + data.id + "&fullname=" + data.fullname);/////
    nameObject.html(data.name + " ");
    name.append(nameObject);
    return name;
}

function BuildAuthorObject(data) {
    var author;
    author = $("<div>");
    author.html("<b>Author:</b>");

    authorObject = $("<a>");
    authorObject.attr("href", APP_PHP_URL + "/page_author.php?author=" + data.author);/////
    authorObject.html(data.author);
    author.append(authorObject);
    return author;
}

function BuildAudioObject(url) {
    var audioObject;
    var sourceObject;
    audioObject = $("<audio>");

    audioObject.attr("controls", "");
    sourceObject = $("<source>");
    sourceObject.attr("src", url);
    sourceObject.attr("type", "audio/ogg");
    audioObject.append(sourceObject);
    return audioObject;
}

function BuildAuthorArray(author) {
    var authorTag;
    authorTag = $("<div>");
    authorTag.html("<b>Author:</b>");

    authorObject = $("<a>");
    authorObject.attr("href", "../page_author.php?author=" + author);
    authorObject.html(author);
    authorTag.append(authorObject);
    return authorTag;
}