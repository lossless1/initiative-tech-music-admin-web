<?php
require_once('./app/php/page_public.php');

class index extends page_public
{
    public function Content()
    {
        $this->AppendScript('auth');
        $this->AppendScript('reglog');
        if(!isset($_SESSION['username'])){
            $mainmenu = "
            <form class='input-group auth-pan' >
                    <input class=\"form-control\" type=\"email\" id=\"user_email\" placeholder='Input email...' autocomplete='off'/>
                    <input class=\"form-control\" type=\"password\" id=\"user_password\" placeholder='Input password...' autocomplete='off'/>
                    <div style='text-align: center'>
                        <button class=\"btn btn-default\" id=\"button_log\" type='submit'>Войти</button>
                    </div>
                    <div id=\"infoLogin\"></div>
            </form>

            <div id=\"name\"></div>
            <div class=\"auth\">
                <div id=\"googleLg\"></div>
                <div id=\"facebookLg\"></div>
                <div id=\"vkLg\"></div>
                <div id=\"twitterLg\"></div>
            </div>
                
               
           ";
        }else{
            header("Location:".APP_PHP_URL."/page_search_public.php");
        }

        return $mainmenu;
    }
}

$page = new index();
$page->DisplayPage();
?>