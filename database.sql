CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_liked_ids` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_idvk` int(11) unsigned DEFAULT NULL,
  `news_added_ids` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags_ids` text COLLATE utf8mb4_unicode_ci,
  `user_googleid` double DEFAULT NULL,
  `user_facebookid` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `author` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `author_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity_subs` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `likesnews`
(
    `id` INT(11) UNSIGNED PRIMARY KEY NOT NULL,
    `news_id` INT(11) UNSIGNED,
    `user_id` INT(11) UNSIGNED
);
CREATE INDEX index_foreignkey_likesnews_news ON likesnews (news_id);
CREATE INDEX index_foreignkey_likesnews_user ON likesnews (user_id);


CREATE TABLE `likessongs`
(
    `id` INT(11) UNSIGNED PRIMARY KEY NOT NULL,
    `id_user` INT(11) UNSIGNED,
    `id_song` INT(11) UNSIGNED
);

CREATE TABLE `music` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `songname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` int(11) unsigned DEFAULT NULL,
  `orgname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `md5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chrono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likes` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idauthor` int(11) unsigned DEFAULT NULL,
  `idtags` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idsong` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idstory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likes` double DEFAULT NULL,
  `addition_time` int(11) unsigned DEFAULT NULL,
  `linksong` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkpicture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name_tag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `usersplaylist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `news` int(11) unsigned DEFAULT NULL,
  `userid` int(11) unsigned DEFAULT NULL,
  `idnews` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;